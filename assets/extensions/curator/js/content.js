/* Inform the backgrund page that
 * this tab should have a page-action */


// Avoid recursive frame insertion...
var extensionOrigin = 'chrome-extension://' + chrome.runtime.id;
if (!location.ancestorOrigins.contains(extensionOrigin)) {
    var iframe = document.createElement('iframe');
    iframe.setAttribute('id', 'muuz-add-popup')
    // Must be declared at web_accessible_resources in manifest.json
    iframe.src = chrome.runtime.getURL('frame.html');
    // Some styles for a fancy sidebar
    iframe.style.cssText = `
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 1 !important;
        z-index: 9999999999;
    `;
    document.body.appendChild(iframe);
}

(function createStyle () {
    var css = `
        #muuz-add-btn {
            cursor: pointer;
            position: absolute;
            top: 12px;
            right: 15px;
            border-radius: 20px;
            display: inline-block;
            color: rgba(240,240,240,1);
            text-shadow: 1px 1px 2px rgba(0,0,0,.4);
            line-height: 24px;
            padding: 3px 24px;
            text-align: center;
            font-size: 16px;
            box-shadow: 1px 1px 3px rgba(0,0,0,.4);
            background-color: #ef7d70;
            background: -webkit-linear-gradient(left, #ef7d70, #e8503e); /* For Safari 5.1 to 6.0 */
            -webkit-transition: .4s; /* Safari */
            transition: .4s;
        }
        #muuz-add-btn:hover{
            background-color: #159945;
            background: -webkit-linear-gradient(left, #e8503e, #e9503e); /* For Safari 5.1 to 6.0 */
        }
        #muuz-add-btn:added{
            background-color: #1ED760;
        }
        #muuz-add-btn img{
            position: relative;
            float: left;
            left: -20px;
            margin-right: -15px;
            height: 24px;
            vertical-align: middle;
            -webkit-filter: drop-shadow1px 1px 2px rgba(0,0,0,.4));
        }
    `,
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');
    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
    head.appendChild(style);
})();



var timeout = null;
var currentYtId = '';

loop()
function loop () {
    setTimeout(function () {
        var videoHolder = document.getElementById('watch-header')
        var videoTitle = document.getElementById('eow-title')

        if (videoHolder && videoTitle) {
            var addToMuusickBtn = document.getElementById('muuz-add-btn')
            // if button has not been created.
            if (!addToMuusickBtn) {
                addToMuusickBtn = document.createElement("div");
                addToMuusickBtn.setAttribute('id', 'muuz-add-btn')
                addToMuusickBtn.appendChild(document.createTextNode("Curate Muusick"));
                videoHolder.appendChild(addToMuusickBtn);
                addToMuusickBtn.addEventListener("click", function(){
                    document.getElementById("muuz-add-popup").style.display = "block";

                    chrome.runtime.sendMessage({
                        from: 'content',
                        subject: 'openIframe',
                        title: document.getElementById('eow-title').innerHTML
                    });
                });
            }

            var ytId = getParameterByName('v', window.location.href);
            if (ytId && ytId !== currentYtId) {
                // load Data
                chrome.runtime.sendMessage({
                    from: 'content',
                    subject: 'init',
                    data: {
                        yt_id: ytId,
                        title: document.getElementById('eow-title').innerHTML
                    }
                }, function (res) {
                    var checkedIcon = document.getElementById("muuz-checked-icon");

                    if (res.video.yt_id && !checkedIcon) {
                        var checkedIcon = document.createElement("img");
                        checkedIcon.setAttribute('id', 'muuz-checked-icon')
                        checkedIcon.setAttribute('src', 'https://muusick.tv/static/myapp/img/checked_icon.png')
                        addToMuusickBtn.appendChild(checkedIcon)
                    }

                    if (!res.video.yt_id && checkedIcon) {
                        addToMuusickBtn.removeChild(checkedIcon)
                    }
                });

                currentYtId = ytId;
            }

        }
        loop()
    }, 1000)
}


addEventListener('message', function(ev) {
    if (ev.data === 'closeIframe') {
        document.getElementById("muuz-add-popup").style.display = "none";
    }
});


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


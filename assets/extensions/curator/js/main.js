

new Vue({
    el: '#app',
    data: {
      default_image: '',
      hostUrl: '',
      username: '',
      my_playlists: [],
      recent_playlists: [],
      trending_playlists: [],
    },
    ready: function () {
        console.log('ready!!')
        chrome.runtime.sendMessage({
            from: 'popup',
            subject: 'browse_lists',
        }, (res) => {
            console.log('Reveive: ', res)
            this.hostUrl = res.host_url
            this.username = res.username
            this.default_image = res.default_image
            this.my_playlists = res.my_playlists
            this.recent_playlists = res.recent_playlists
            this.trending_playlists = res.trending_playlists
        });
    },
    methods: {
        init: function () {

        }
    }
})
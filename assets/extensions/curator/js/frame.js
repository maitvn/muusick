
var data = {
  showNewPlaylist: false,
  showLogin: false,
  isStaff: false,
  currentVideoId: '',
  currentVideoTitle: '',
  newlist: {
    name: '',
    description: '',
    msg: ''
  },
  defaultImage: '',
  genres: [],
  instruments: [],
  hotTags: [],
  playlists:[],
  videoInfo: {},
  tagInput: '',
  tagSuggestions: [],
  artistInput: '',
  artistSuggestions: [],
  onArtistFocus: false,
  onTagFocus: false
}

chrome.runtime.onMessage.addListener(function (msg, sender, response) {
    if ((msg.from === 'background') && (msg.subject === 'init')) {
        data.currentVideoId = msg.yt_id
        data.isStaff = msg.data.is_staff
        data.genres = msg.data.genres
        data.playlists = msg.data.user_playlists
        data.videoInfo = msg.data.video
        data.instruments = msg.data.instruments
        data.hotTags = msg.data.hot_tags
        return true;
    }

    if ((msg.from === 'background') && (msg.subject === 'openIframe')) {
        data.currentVideoTitle = msg.title;
        return true;
    }

    if ((msg.from === 'background') && (msg.subject === 'auth_required')) {
        data.showLogin = true
        return true
    }
});


new Vue({
    el: '#muuz-add-popup',
    data: data,
    watch: {
        'tagInput': function (val) {
            if (val.trim() === '') {
                this.tagSuggestions = [];
                return true;
            }
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'get_tag_suggestions',
                data: {
                    text: val
                }
            }, (res) => {
                this.tagSuggestions = res.tags
            });
        },
        'artistInput': function (val) {
            if (val.trim() === '') {
                this.artistSuggestions = [];
                return true;
            }
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'get_artist_suggestions',
                data: {
                    text: val
                }
            }, (res) => {
                this.artistSuggestions = res.artists
            });
        }
    },
    ready: function () {
        $('.muuz-playlist-wrapper').perfectScrollbar();
        $('.scroll').perfectScrollbar();
    },
    methods: {
        frameClick: function (e) {
            if (e.target === this.$el) {
                this.closePopup()
            }
        },
        outArtistFocus: function () {
          setTimeout( () => {
            this.onArtistFocus = false
          }, 100)
        },
        outTagFocus: function () {
          setTimeout( () => {
            this.onTagFocus = false
          }, 100)
        },
        closePopup: function () {
            parent.postMessage('closeIframe', '*');
        },
        add2List: function (listId) {
            var item = _.find(this.playlists, {id: listId});
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'add_2_list',
                data: {
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                    playlist_id: listId,
                    flag: !item.is_added ? 1 : 0
                }
            }, (res) => {
                item.is_added = !item.is_added;
            });
        },
        createList: function () {
            if (this.newlist.name === '') {
                this.newlist.msg = 'Please enter playlist name!'
                return true
            }
            if (this.newlist.description.length < 10) {
                this.newlist.msg = 'Description is too short, Give more words for your awesome playlist!'
                return true
            }
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'create_list',
                data: {
                    name: this.newlist.name,
                    description: this.newlist.description
                }
            }, (res) => {
                this.playlists.unshift(res.playlist);
                this.showNewPlaylist = false
            });
        },
        deleteTag: function (tagName) {
            NProgress.start();
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'update_tags',
                data: {
                    flag: 0,
                    tag_name: tagName,
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                }
            }, (res) => {
                NProgress.done();
                var idx = _.findIndex(this.videoInfo.tags, {name: tagName});
                idx >= 0 && this.videoInfo.tags.splice(idx, 1)
            });
        },
        selectTagSuggestion: function (name) {
            this.tagInput = ''

            setTimeout(() => {
                this.tagSuggestions = []
            }, 100)

            NProgress.start();
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'update_tags',
                data: {
                    flag: 1,
                    tag_name: name,
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                }
            }, (res) => {
                NProgress.done();
                if (res) {
                    this.videoInfo.tags.push(res);
                }
            });
        },
        enterTag: function () {
            this.tagInput = this.tagInput.trim().replace(/\b\w/g, l => l.toUpperCase())
            if (this.tagInput === '') {
                return true;
            }
            setTimeout(() => {
                this.tagSuggestions = []
            }, 100)
            NProgress.start();
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'update_tags',
                data: {
                    flag: 1,
                    tag_name: this.tagInput,
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                }
            }, (res) => {
                NProgress.done();
                this.tagInput = ''
                if (res) {
                    data.videoInfo.tags.push(res);
                }
            });
        },
        deleteArtistTag: function (artistName) {
            NProgress.start();
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'update_artists',
                data: {
                    flag: 0,
                    artist_name: artistName,
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                }
            }, (res) => {
                NProgress.done();
                var idx = _.findIndex(this.videoInfo.artists, {name: artistName});
                idx >= 0 && this.videoInfo.artists.splice(idx, 1)
            });
        },
        selectArtistSuggestion: function (name) {
            this.artistInput = ''
            setTimeout(() => {
                this.artistSuggestions = []
            }, 100)
            NProgress.start();
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'update_artists',
                data: {
                    flag: 1,
                    artist_name: name,
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                }
            }, (res) => {
                NProgress.done();

                if (res) {
                    this.videoInfo.artists.push(res);
                }
            });
        },
        enterArtistTag: function () {
            this.artistInput = this.artistInput.trim().replace(/\b\w/g, l => l.toUpperCase())
            if (this.artistInput === '') {
                return true;
            }
            setTimeout(() => {
                this.artistSuggestions = []
            }, 100)

            NProgress.start();
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'update_artists',
                data: {
                    flag: 1,
                    artist_name: this.artistInput,
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                }
            }, (res) => {
                NProgress.done();
                this.artistInput = ''
                if (res) {
                    this.videoInfo.artists.push(res);
                }
            });
        },
        toggleGenre: function (id) {
            NProgress.start();
            var item = _.find(this.genres, {id: id})
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'update_genres',
                data: {
                    flag: item.is_added ? 0 : 1,
                    genre_id: id,
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                }
            }, (res) => {
                NProgress.done();
                item.is_added = !item.is_added;
            });
        },
        toggleTag: function (type, id) {
            NProgress.start();
            var item = type === 'instruments' ? _.find(this.instruments, {id: id}) : _.find(this.hotTags, {id: id})
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'update_tags',
                data: {
                    flag: item.is_added ? 0 : 1,
                    tag_id: id,
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                }
            }, (res) => {
                NProgress.done();
                item.is_added = !item.is_added;
            });
        },
        addToPlaylist: function (listId) {
            NProgress.start();
            var item = _.find(this.playlists, {id: listId})

            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'add_2_list',
                data: {
                    flag: item.is_added ? 0 : 1,
                    playlist_id: listId,
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                }
            }, (res) => {
                NProgress.done();
                item.is_added = !item.is_added;
            });
        }
    }
})
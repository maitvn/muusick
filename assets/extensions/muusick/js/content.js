/* Inform the backgrund page that
 * this tab should have a page-action */


// Avoid recursive frame insertion...
var extensionOrigin = 'chrome-extension://' + chrome.runtime.id;
if (!location.ancestorOrigins.contains(extensionOrigin)) {
    var iframe = document.createElement('iframe');
    iframe.setAttribute('id', 'muuz-add-popup')
    // Must be declared at web_accessible_resources in manifest.json
    iframe.src = chrome.runtime.getURL('frame.html');

    // Some styles for a fancy sidebar
    iframe.style.cssText = `
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 1 !important;
        z-index: 9999999999;
    `;

    document.body.appendChild(iframe);
}

(function createStyle () {
    var css = `
        #muuz-add-btn {
            cursor: pointer;
            position: absolute;
            top: 12px;
            right: 15px;
            border-radius: 20px;
            display: inline-block;
            color: rgba(240,240,240,1);
            text-shadow: 1px 1px 2px rgba(0,0,0,.4);
            line-height: 24px;
            padding: 3px 24px;
            text-align: center;
            font-size: 16px;
            box-shadow: 1px 1px 3px rgba(0,0,0,.4);
            background-color: #1ED760;
            background: -webkit-linear-gradient(left, #4FB168, #1ED760); /* For Safari 5.1 to 6.0 */
            -webkit-transition: .4s; /* Safari */
            transition: .4s;
         }
        #muuz-add-btn:hover{
            background-color: #159945;
            background: -webkit-linear-gradient(left, rgb(14,178,154), #4FB168); /* For Safari 5.1 to 6.0 */
         }
        #muuz-add-btn:added{
            background-color: #1ED760;
         }
    `,
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');

    style.type = 'text/css';
    if (style.styleSheet){
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
    head.appendChild(style);
})();



var currentYtId = '';

loop();
function loop () {
    setTimeout(function () {
        var videoHolder = document.getElementById('watch-header')
        var videoTitle = document.getElementById('eow-title')

        if (videoHolder && videoTitle) {
            var addToMuusickBtn = document.getElementById('muuz-add-btn')
            // if button has not been created.
            if (!addToMuusickBtn) {
                addToMuusickBtn = document.createElement("div");
                addToMuusickBtn.setAttribute('id', 'muuz-add-btn')
                addToMuusickBtn.appendChild(document.createTextNode("Add to Muusick"));
                videoHolder.appendChild(addToMuusickBtn);
                addToMuusickBtn.addEventListener("click", function(){
                    document.getElementById("muuz-add-popup").style.display = "block";
                    loadIframeData ()
                });
            }
        }

        // loop
        loop()
    }, 1000)
}


function loadIframeData () {
    chrome.runtime.sendMessage({
        from: 'content',
        subject: 'init',
        data: {
            yt_id: getParameterByName('v', window.location.href),
            title: document.getElementById('eow-title').innerHTML
        }
    });
}



function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


addEventListener('message', function(ev) {
    if (ev.data === 'closeIframe') {
        document.getElementById("muuz-add-popup").style.display = "none";
    }
});

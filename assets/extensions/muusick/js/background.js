
//const SERVER = 'http://localhost:8000/'
const SERVER = 'https://muusick.tv/';


function request (method, url, data, successCb, errorCb) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
         if (xmlhttp.status === 200) {
             try {
                  var response = JSON.parse(xmlhttp.responseText);
                  successCb && successCb(response);
              }
              catch(err) {
                errorCb && errorCb();
              }
         } else if (xmlhttp.status === 310) {
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
              chrome.tabs.sendMessage(tabs[0].id, {
                from: "background",
                subject: "auth_required",
                data: data
              });
            });
         }
      }
    };

    var str = "";
    for (var key in data) {
        if (str != "") {
            str += "&";
        }
        str += key + "=" + encodeURIComponent(data[key]);
    }
    if (method === 'POST') {
        xmlhttp.open(method, SERVER + url, true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    } else {
        url = str ? url + '?' + str : url;
        xmlhttp.open(method, SERVER + url, true);
    }
    xmlhttp.send(str);
}


chrome.runtime.onMessage.addListener(function (msg, sender, response) {

    if ((msg.from === 'content') && (msg.subject === 'init')) {
        /* Enable the page-action for the requesting tab */

        request('GET', 'api/ext/lists', msg.data, function (res) {
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
              chrome.tabs.sendMessage(tabs[0].id, {
                from: "background",
                subject: "init",
                data: res,
                yt_id: msg.data.yt_id,
                title: msg.data.title
              });
            });
        });

        return true
    }


    if ((msg.from === 'iframe') && (msg.subject === 'add_2_list')) {
        request('POST', 'api/ext/add_2_list', msg.data, function (res) {
            response(res)
        });
        return true;
    }

    if ((msg.from === 'iframe') && (msg.subject === 'create_list')) {
        request('POST', 'api/ext/create_list', msg.data, (res) => {
            response(res)
        });
        return true;
    }

    if ((msg.from === 'popup') && (msg.subject === 'browse_lists')) {
        request('GET', 'api/ext/browse_lists', {}, (res) => {
            response(res)
        });
        return true;
    }
});



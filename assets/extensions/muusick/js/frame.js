
var data = {
  showNewPlaylist: true,
  showLogin: false,
  showAddNew: false,
  currentVideoId: '',
  currentVideoTitle: '',
  playlists: [],
  newlist: {
    name: '',
    description: '',
    msg: ''
  },
  defaultImage: ''
}

chrome.runtime.onMessage.addListener(function (msg, sender, response) {
    if ((msg.from === 'background') && (msg.subject === 'init')) {
        data.currentVideoId = msg.yt_id;
        data.currentVideoTitle = msg.title;

        if (msg.data) {
            data.playlists = msg.data.playlists;
            data.defaultImage = msg.data.default_image
            this.showLogin = false
        }
        return true
    }

    if ((msg.from === 'background') && (msg.subject === 'auth_required')) {
        data.currentVideoId = msg.data.yt_id;
        data.currentVideoTitle = msg.data.title;

        data.showLogin = true
        return true
    }
});


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


new Vue({
    el: '#muuz-add-popup',
    data: data,
    ready: function () {
        $('.muuz-playlist-wrapper').perfectScrollbar();
    },
    methods: {
        frameClick: function (e) {
            if (e.target === this.$el) {
                this.closePopup();
            }
        },
        add2List: function (listId) {
            var item = _.find(this.playlists, {id: listId});
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'add_2_list',
                data: {
                    yt_id: this.currentVideoId,
                    title: this.currentVideoTitle,
                    playlist_id: listId,
                    flag: !item.is_added ? 1 : 0
                }
            }, (res) => {
                item.is_added = !item.is_added;
            });
        },
        closePopup: function () {
            parent.postMessage('closeIframe', '*');
        },
        createList: function () {
            if (this.newlist.name === '') {
                this.newlist.msg = 'Please enter playlist name!'
                return true
            }
            if (this.newlist.name === '') {
                this.newlist.description = 'Please enter playlist description!'
                return true
            }
            chrome.runtime.sendMessage({
                from: 'iframe',
                subject: 'create_list',
                data: {
                    name: this.newlist.name,
                    description: this.newlist.description
                }
            }, (res) => {
                this.playlists.unshift(res.playlist);
                this.showAddNew = false
            });
        }
    }
})
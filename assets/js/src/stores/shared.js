import http from '../services/http'
import user from './user'
import {each} from 'lodash'
import DefaultVideo from '../stubs/video'
import DefaultTag from '../stubs/tag'

export default {
  state: {
    user: {},
    tags: [DefaultTag],
    sliderVideos: [DefaultVideo],
    videos: [],
    browse: []
  },
  init (successCb) {
    var self = this
    var tagLoaded = false
    var userLoaded = false
    http.getTags(function (res) {
      each(res.tags, function (item) {
        item.active = false
      })
      self.state.tags = res.tags
      tagLoaded = true
      tagLoaded && userLoaded && successCb && successCb()
    })
    user.getLoginInfo(function () {
      userLoaded = true
      tagLoaded && userLoaded && successCb && successCb()
    })
  },

  getSuggestion: function (keyword, callback) {
    http.get('/api/search_suggest', {keyword: keyword}, function (res) {
      callback && callback(res)
    })
  },

  loadSliderVideos: function (callback) {
    var self = this
    http.getSliderVideos(function (res) {
      self.state.sliderVideos = res.videos
      callback && callback()
    })
  },

  loadTagCombination: function (callback) {
    http.getTagCombinations(function (res) {
      callback && callback(res)
    })
  },

  loadVideos: function (tags, callback) {
    // var self = this
    http.getVideosByTags(tags, function (res) {
      // self.state.videos = res.videos
      callback && callback(res)
    })
  },

  loadVideo: function (ytId, callback) {
    http.getVideo(ytId, function (res) {
      callback && callback(res)
    })
  },

  browse: function (order, callback) {
    var self = this
    http.browse(order, function (res) {
      self.state.browse = res.genres
      callback && callback()
    })
  },

  browseCategory: function (categoryId, callback) {
    http.browseCategory(categoryId, function (res) {
      callback && callback(res)
    })
  }
}

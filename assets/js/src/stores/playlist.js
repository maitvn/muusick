import http from '../services/http'
import defaultPlaylists from '../stubs/playlist'
export default {
  state: {
    defaultPlaylists: [defaultPlaylists]
  },
  browsePlaylists: function (callback) {
    http.get('/api/browse_playlists', {}, function (res) {
      callback && callback(res)
    })
  },
  getPlaylistInfo: function (id, callback) {
    http.get('/api/get_playlist_data', {id: id}, function (res) {
      callback && callback(res)
    })
  },
  loadPlaylists: function (ytId, callback) {
    http.get('/api/get_related_playlists', {yt_id: ytId}, function (res) {
      callback && callback(res)
    })
  },
  createPlaylist: function (name, description, callback) {
    http.get('/api/create_playlist', {name: name, description: description}, function (res) {
      callback && callback(res)
    })
  },
  addToList: function (ytId, listId, callback) {
    http.get('/api/add_to_playlist', {yt_id: ytId, playlist_id: listId}, function (res) {
      callback && callback(res)
    })
  }
}

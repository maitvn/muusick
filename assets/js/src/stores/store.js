import http from '../services/http'

export default {
  state: {
    isPlayerReady: false,
    user: {},
    userPlaylists: {},
    collaboratePlaylists: [],
    genres: [],
    instrumentTags: [],
    hotTags: [],
    favorites: {},
    history: {},
    features: {},
    trending: {},
    personalized: {},
    newAdded: [],
    instruments: []
  },

  init: function (callback) {
    http.get('/api/initial', {}, (res) => {
      this.state.user = res.user
      this.state.userPlaylists = res.userPlaylists
      this.state.collaboratePlaylists = res.collaboratePlaylists
      this.state.genres = res.genres
      this.state.instrumentTags = res.instrumentTags
      this.state.hotTags = res.hotTags
      this.state.favorites = res.favorites
      this.state.history = res.history
      this.state.features = res.features
      this.state.trending = res.trending
      this.state.personalized = res.personalized
      this.state.newAdded = res.newAdded
      this.state.instruments = res.instruments
      callback && callback()
    })
  }
}

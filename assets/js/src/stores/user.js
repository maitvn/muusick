import http from '../services/http'
import $ from 'jquery'

export default {
  state: {
    user: {
      id: 0,
      username: '',
      last_name: '',
      first_name: ''
    },
    isAuthenticated: false
  },
  init (successCb) {
  },
  getLoginInfo: function (callback) {
    var self = this
    http.get('/api/get_login_info', {}, function (res) {
      self.state.isAuthenticated = res.is_authenticated
      self.state.user = res.user
      callback && callback()
    })
  },
  login: function (callback) {
    var self = this
    http.get('/api/login', {}, function (res) {
      self.state.isAuthenticated = res.is_authenticated
      self.state.user = res.user
      callback && callback()
    })
  },
  getUserProfile: function (callback) {
    http.get('/api/get_user_profile', {}, function (res) {
      callback && callback(res)
    })
  },
  updateProfile: function (data, callback) {
    http.post('/api/update_profile', data, function (res) {
      callback && callback(res)
    })
  },
  uploadProfilePicture: function (data, callback) {
    // http.post('/api/upload_profile_picture', data, function (res) {
    //   callback && callback(res)
    // })

    $.ajax({
      url: '/api/upload_profile_picture',
      type: 'POST',
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      success: function (data) {
        callback && callback(data)
      }
    })
  },
  getUserVideos: function (username, tagId, callback) {
    var data = {
      username: username,
      tag_id: tagId
    }
    http.get('/api/get_user_videos', data, function (res) {
      callback && callback(res)
    })
  },
  checkFollowUser: function (username, callback) {
    http.get('/api/check_follow_user', {username: username}, function (res) {
      callback && callback(res)
    })
  },
  followPlaylist: function (playlistId, flag, callback) {
    var data = {
      playlist_id: playlistId,
      flag: flag
    }
    http.get('/api/follow_playlist', data, function (res) {
      callback && callback(res)
    })
  },
  followUser: function (username, flag, callback) {
    var data = {
      username: username,
      flag: flag
    }
    http.get('/api/follow_user', data, function (res) {
      callback && callback(res)
    })
  },
  followCategory: function (categoryId, flag, callback) {
    var data = {
      category_id: categoryId,
      flag: flag
    }
    http.get('/api/follow_category', data, function (res) {
      callback && callback(res)
    })
  }
}

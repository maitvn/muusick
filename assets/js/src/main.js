import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import ArtistView from './components/artist/main'
import UserView from './components/user/main'
import PlaylistView from './components/playlist/main'
import BrowseView from './components/browse/main'
import HistoryView from './components/history/main'
import FavoriteView from './components/favorite/main'
import PlaylistListView from './components/list/playlist'
import VideoListView from './components/list/video'
import ArtistListView from './components/list/artist'
import FollowingPlaylists from './components/following/main'
import BrowsePlaylistsView from './components/browse-playlists/main'
import GeneralBrowse from './components/browse/general-browse'
import DetailBrowse from './components/browse/detail-browse'
import HomeView from './components/home/main'
import SearchView from './components/search/main'
import ConfirmInvitation from './components/confirm-invite/main'
import PlayerItem from './components/player/main'
import VueLazyload from 'vue-lazyload'

Vue.use(VueRouter)

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: 'https://muusick.tv/static/myapp/img/LoadingImage.png',
  loading: 'https://muusick.tv/static/myapp/img/LoadingImage.png',
  attempt: 1,
  listenEvents: [ 'onscroll', 'scroll', 'wheel', 'mousewheel', 'resize', 'animationend', 'transitionend' ]
})

var router = new VueRouter()

router.map({
  '': {
    name: 'home',
    component: HomeView
  },
  '_=_': {
    name: 'home',
    component: HomeView
  },
  '/browse': {
    name: 'browse',
    component: BrowseView,
    subRoutes: {
      '/': {
        component: GeneralBrowse
      },
      '/:type/:id': {
        name: 'browseDetail',
        component: DetailBrowse
      }
    }
  },
  '/playlist/:slug': {
    name: 'playlist',
    component: PlaylistView
  },
  '/search': {
    name: 'search',
    component: SearchView
  },
  '/list/playlist/:param': {
    name: 'playlistList',
    component: PlaylistListView
  },
  '/list/video/:param': {
    name: 'videoList',
    component: VideoListView
  },
  '/list/artist/:param': {
    name: 'artistList',
    component: ArtistListView
  },
  '/history': {
    name: 'history',
    component: HistoryView
  },
  '/favorite': {
    name: 'favorite',
    component: FavoriteView
  },
  '/following/playlists': {
    name: 'followingPlaylists',
    component: FollowingPlaylists
  },
  '/playlists': {
    name: 'browsePlaylist',
    component: BrowsePlaylistsView
  },
  '/player/:type/:slug': {
    name: 'player',
    component: PlayerItem
  },
  '/user/:username': {
    name: 'user',
    component: UserView
  },
  '/profile': {
    name: 'profile',
    component: UserView
  },
  '/artist/:name': {
    name: 'artist',
    component: ArtistView
  },
  '/confirm-invite/:key': {
    name: 'confirmInvite',
    component: ConfirmInvitation
  }
})

router.start(App, '#container')

export default {
  id: '',
  title: '',
  yt_id: '',
  tags: '',
  category: {
    id: '',
    name: '',
    songs_count: 0
  },
  curator: {
    id: '',
    username: '',
    first_name: '',
    last_name: ''
  }
}

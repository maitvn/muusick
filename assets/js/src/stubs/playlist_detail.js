import defaultVideo from './video'

export default {
  id: 0,
  name: '',
  is_favorite: false,
  videos: [defaultVideo],
  created_at: ''
}

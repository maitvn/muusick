import {get, post, ajax} from 'jquery'
import NProgress from 'nprogress'
export default {
  get: function (url, data, successCb) {
    NProgress.start()
    get(url, data, function (res) {
      NProgress.done()
      successCb && successCb(res)
    })
  },
  _get: function (url, data, successCb) {
    get(url, data, function (res) {
      successCb && successCb(res)
    })
  },
  post: function (url, data, successCb, errorCb) {
    NProgress.start()
    post(url, data, function (res) {
      NProgress.done()
      successCb && successCb(res)
    }).fail(function (res) {
      NProgress.done()
      errorCb && errorCb(res)
    })
  },
  _post: function (url, data, successCb, errorCb) {
    post(url, data, function (res) {
      successCb && successCb(res)
    }).fail(function (res) {
      errorCb && errorCb(res)
    })
  },
  upload: function (url, data, successCb) {
    ajax({
      url: url,
      type: 'POST',
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      success: function (data) {
        successCb && successCb(data)
      }
    })
  }
}


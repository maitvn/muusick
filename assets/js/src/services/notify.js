export default {
  permission: false,

  askPermission: function () {
    // request permission on page load
    if (!window.Notification) {
      return
    }
    if (window.Notification.permission !== 'granted') {
      window.Notification.requestPermission()
    }
  },
  play: function (ytId, message) {
    if (window.Notification.permission === 'granted') {
      var notification = new window.Notification('Playing', {
        icon: 'https://img.youtube.com/vi/' + ytId + '/default.jpg',
        body: message
      })

      notification.onclick = function () {
        this.close()
      }

      setTimeout(() => {
        notification.close()
      }, 3000)
    }
  },
  notify: function (title, message) {
    if (window.Notification.permission === 'granted') {
      var notification = new window.Notification(title, {
        icon: 'https://muusick.tv/static/myapp/img/muusick_notify_icon.png',
        body: message
      })

      notification.onclick = function () {
        this.close()
      }

      setTimeout(() => {
        notification.close()
      }, 3000)
    }
  }
}

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^video_check', views.video_check),

    url(r'^api/import_playlist', views.import_playlist),
]

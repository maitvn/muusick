from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from myapp.models import *
from django.contrib.auth.decorators import user_passes_test
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import datetime
import urllib2, urllib
from bs4 import BeautifulSoup

@login_required
@user_passes_test(lambda u: u.is_superuser)
def video_check(request):
    return render(request, 'backoffice/video_check.html', {
        'videos': Video.objects.all()
    })


@csrf_exempt
@login_required
@user_passes_test(lambda u: u.is_superuser)
def import_playlist(request):
    if 'id' not in request.POST:
        return JsonResponse({
            'message': 'ID is not set'
        })

    object = ImportPlaylist.objects.get(pk=int(request.POST['id']))

    if not object:
        return JsonResponse({
            'message': 'ID is not set'
        })

    extracted_videos = []
    try:
        page = urllib2.urlopen(object.yt_playlist_url).read()
        soup = BeautifulSoup(page, 'html.parser')
        videos = soup.find_all("li", class_="yt-uix-scroller-scroll-unit")

        for video in videos:
            extracted_videos.append({
                'yt_id': video['data-video-id'],
                'title': video['data-video-title'],
            })
    except:
        object.status = 2
        object.import_at = datetime.datetime.now()
        object.save()
        return JsonResponse({
            'message': 'Import youtube error!'
        })

    if extracted_videos:
        playlist = None
        if object.type == 0 and object.new_playlist_name and object.new_playlist_description:
            playlist = Playlist.objects.create(
                name=object.new_playlist_name,
                description=object.new_playlist_description,
                creator=object.creator
            )
            object.type = 1
            object.playlist = playlist
        elif object.playlist:
            playlist = object.playlist
        else:
            return JsonResponse({
                'message': 'No playlist !'
            })

        if object.country:
            playlist.country = object.country

        if object.artist:
            playlist.artists.add(object.artist)

        for tag in object.tags.all():
            playlist.tags.add(tag)

        for genre in object.genres.all():
            playlist.genres.add(genre)

        playlist.save()

        if object.is_override:
            PlaylistVideo.objects.filter(playlist_id=playlist.id).delete()

        for video in extracted_videos:
            video_object = Video.objects.filter(yt_id=video['yt_id']).first()
            if not video_object:
                video_object = Video.objects.create(
                    yt_id=video['yt_id'],
                    title=video['title']
                )

            if object.country:
                video_object.country = object.country

            if object.artist:
                video_object.artists.add(object.artist)

            for tag in object.tags.all():
                video_object.tags.add(tag)

            for genre in object.genres.all():
                video_object.genres.add(genre)

            video_object.save()

            PlaylistVideo.objects.update_or_create(
                playlist_id=playlist.id,
                video_id=video_object.id
            )

    object.status = 1
    object.import_at = datetime.datetime.now()
    object.save()

    return JsonResponse({
        'message': 'Imported video count: %d' % len(extracted_videos)
    })

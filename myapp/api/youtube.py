#!/usr/bin/python

from apiclient.discovery import build
from apiclient.errors import HttpError
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


# from oauth2client.tools import argparser


# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = "AIzaSyBLBy_DG9RR2fm8trxTVGuFw4esv-DlBXQ"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"


@csrf_exempt
def search(request):
    keyword = request.GET['q']

    videos = yt_search(keyword, 20)

    return JsonResponse({
        'query': keyword,
        'videos': videos
    })


def yt_search(keyword, max_results):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                    developerKey=DEVELOPER_KEY)

    # Call the search.list method to retrieve results matching the specified
    # query term.
    search_response = youtube.search().list(
        q=keyword,
        part="id,snippet",
        type="video",
        videoCategoryId='10',
        maxResults=max_results
    ).execute()

    videos = []

    # Add each result to the appropriate list, and then display the lists of
    # matching videos, channels, and playlists.
    for search_result in search_response.get("items", []):
        if search_result["id"]["kind"] == "youtube#video":
            videos.append({'title': search_result["snippet"]["title"], 'yt_id': search_result["id"]["videoId"]})

    # print "Videos:\n", "\n".join(videos), "\n"
    return videos


def yt_related_video(yt_id, n):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                    developerKey=DEVELOPER_KEY)

    # Call the search.list method to retrieve results matching the specified
    # query term.
    search_response = youtube.search().list(
        part="id,snippet",
        type="video",
        videoCategoryId='10',
        maxResults=n,
        relatedToVideoId=yt_id
    ).execute()

    videos = []

    # Add each result to the appropriate list, and then display the lists of
    # matching videos, channels, and playlists.
    for search_result in search_response.get("items", []):
        if search_result["id"]["kind"] == "youtube#video":
            videos.append({'title': search_result["snippet"]["title"], 'yt_id': search_result["id"]["videoId"]})

    # print "Videos:\n", "\n".join(videos), "\n"
    return videos


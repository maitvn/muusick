from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from myapp.models import Mood, Video
from myapp.models import FollowUser, Playlist, PlaylistVideo
from myapp.serializers import *
from django.core.exceptions import ObjectDoesNotExist
from django import forms
from django.core.files import File
import os
from PIL import Image
import StringIO
from django.views.decorators.cache import cache_page
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.core.cache import cache


@csrf_exempt
@api_view(['GET'])
@cache_page(10)
def get_playlist_data(request):
    """
    Get playlist detail information (for playing)
    :param request:
    :return:
    """
    playlist = Playlist.objects.get(slug=request.GET['slug'])

    playlist_data = cache.get('playlist_data-%d' % playlist.id)
    if not playlist_data or playlist.creator == request.user:
        playlist_data = PlaylistDetailSerializer(
            playlist,
            many=False,
            context={'request': request}).data

        videos = VideoDetailSerializer(
                playlist.videos.all().prefetch_related('tags', 'artists', 'genres'),
                many=True,
                context={'request': request}
            ).data

        playlist_data['videos'] = videos

        cache.set('playlist_data-%d' % playlist.id, playlist_data, 60 * 60)

    return Response({
        'data': playlist_data,
    })


@csrf_exempt
@api_view(['GET'])
def get_playlist_private_data(request):
    """
    Get playlist detail information (for playing)
    :param request:
    :return:
    """
    playlist = Playlist.objects.get(slug=request.GET['slug'])

    contributors = ContributorSerializer(
        Collaborator.objects.filter(playlist=playlist).select_related('user', 'user__profile'),
        many=True,
        context={'request': request}).data

    is_guest = request.user.is_anonymous()

    return Response({
        'is_owned': request.user == playlist.creator if not is_guest else False,
        'is_following': FollowPlaylist.objects.filter(
            user=request.user,
            playlist=playlist).exists() if not is_guest else False,
        'is_contributor': Collaborator.objects.filter(
            user=request.user,
            playlist=playlist).exists() if not is_guest else False,
        'contributors': contributors
    })


@login_required
@csrf_exempt
def create_playlist(request):
    """
    Create new playlist!
    """
    if request.method == 'POST':
        playlist = Playlist(creator=request.user,
                            name=request.POST['name'],
                            description=request.POST['description']
                            )
        playlist.save()

        if 'image' in request.FILES:
            file = request.FILES['image']
            filename, file_extension = os.path.splitext(file.name)
            im = Image.open(file)
            im.thumbnail((600, 600), Image.ANTIALIAS)
            image_file = StringIO.StringIO()
            ext = file_extension[1:len(file_extension)].upper()
            ext = ext if ext == 'PNG' else 'JPEG'
            im.save(image_file, ext, quality=90)
            playlist.image.save(file.name, image_file)

        serializer = PlaylistSerializer(playlist, many=False, context={'request': request})
        return JsonResponse(serializer.data)
    return JsonResponse({})


@login_required
@csrf_exempt
def update_playlist(request):
    """
    Update playlist information
     Upload image for playlist as well.
    """
    if request.method == 'POST':
        playlist = Playlist.objects.get(id=int(request.POST['id']))
        playlist.name = request.POST['name']
        playlist.description = request.POST['description']
        playlist.save()
        serializer = PlaylistSerializer(playlist, many=False, context={'request': request})

        if 'image' in request.FILES:
            file = request.FILES['image']
            filename, file_extension = os.path.splitext(file.name)
            im = Image.open(file)
            im.thumbnail((600, 600), Image.ANTIALIAS)
            image_file = StringIO.StringIO()
            ext = file_extension[1:len(file_extension)].upper()
            ext = ext if ext == 'PNG' else 'JPEG'
            im.save(image_file, ext, quality=90)
            playlist.image.save(file.name, image_file)

        return JsonResponse(serializer.data)
    return JsonResponse({})


@login_required
@csrf_exempt
def publish_playlist(request):
    """
    Publish/ unpublish a playlist
    """
    if request.method == 'POST':
        try:
            playlist = Playlist.objects.get(id=int(request.POST['id']))

            if playlist.videos.count() < 10 or not playlist.image:
                return JsonResponse({'message': '< 4 songs'}, status=310)

            playlist.is_public = bool(int(request.POST['is_public']))
            playlist.save()
            print playlist.is_public
            return JsonResponse({'is_public': playlist.is_public})
        except ObjectDoesNotExist:
            return JsonResponse({}, status=500)
    return JsonResponse({}, status=500)


@login_required
@csrf_exempt
def delete_playlist(request):
    """
    Delete a playlist!
    """
    if request.method == 'POST':
        try:
            playlist = Playlist.objects.get(id=int(request.POST['id']))
            if playlist.creator != request.user:
                return JsonResponse({}, status=500)
            else:
                playlist.delete()
                return JsonResponse({}, status=200)

        except ObjectDoesNotExist:
            return JsonResponse({}, status=500)
    return JsonResponse({}, status=500)


@login_required
@csrf_exempt
def remove_from_playlist(request):
    """
    Remove from playlist
    """
    try:
        playlist_id = int(request.POST['playlist_id'])
        yt_id = request.POST['yt_id']
        playlist = Playlist.objects.get(pk=playlist_id)
        video = Video.objects.get(yt_id=yt_id)
        playlist_item = PlaylistVideo.objects.get(playlist=playlist, video=video)
        if playlist_item:
            playlist_item.delete()

    except ObjectDoesNotExist:
        return JsonResponse({}, status=500)
    return JsonResponse({})

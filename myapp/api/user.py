from PIL import Image
import StringIO
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from myapp.serializers import *
from django.core.mail import send_mail
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.conf import settings
import re
from django.db.models import Q
from django.contrib.staticfiles.templatetags.staticfiles import static
from rest_framework.response import Response
from rest_framework.decorators import api_view


@login_required
@csrf_exempt
def update_profile(request):
    """
    Update user profile : including change profile picture
    :param request:
    :return:
    """
    if request.method == 'POST':
        user = request.user
        user.save()
        profile = Profile.objects.filter(user=user).first()
        if not profile:
            profile = Profile.objects.create(
                user=user,
            )
        profile.bio = request.POST['bio']
        profile.fullname = request.POST['fullname']
        profile.save()
        if 'image' in request.FILES:
            file = request.FILES['image']
            filename, file_extension = os.path.splitext(file.name)
            im = Image.open(file)
            im.thumbnail((300, 300), Image.ANTIALIAS)
            image_file = StringIO.StringIO()
            ext = file_extension[1:len(file_extension)].upper()
            ext = ext if ext == 'PNG' else 'JPEG'
            im.save(image_file, ext, quality=90)
            profile.picture.save(file.name, image_file)

        user_serializer = UserDetailSerializer(user, many=False, context={'request': request})
        return JsonResponse(user_serializer.data)
    return JsonResponse({})


@csrf_exempt
def get_user_profile(request):
    """
    Get user profile
    ... Used to render profile page.
    :param request:
    :return:
    """
    if request.GET['username']:
        user = User.objects.get(username=request.GET['username'])
        my_playlists = PlaylistSerializer(Playlist.objects.public().filter(creator=user), many=True,
                                          context={'request': request})
    else:
        user = request.user
        my_playlists = PlaylistSerializer(Playlist.objects.filter(creator=user), many=True,
                                          context={'request': request})

    user_serializer = UserDetailSerializer(user, many=False, context={'request': request})
    _following_users = [f.to_user for f in FollowUser.objects.filter(from_user=user)]
    _followed_users = [f.from_user for f in FollowUser.objects.filter(to_user=user)]
    following_users = UserDetailSerializer(_following_users, many=True, context={'request': request})
    followers = UserDetailSerializer(_followed_users, many=True, context={'request': request})
    follow_playlists = PlaylistSerializer(Playlist.objects.filter(following=user), many=True, context={'request': request})
    return JsonResponse({
        'following_categories': [],
        'following_users': following_users.data,
        'followers': followers.data,
        'my_playlists': my_playlists.data,
        'following_playlists': follow_playlists.data,
        'profile': user == request.user,  # true: owner / profile page
        'user_info': user_serializer.data
    })


@login_required
@csrf_exempt
def follow_user(request):
    """
    Follow user
    :param request:
    :return:
    """
    from_user = request.user
    username = request.GET['username']
    on_flag = int(request.GET['flag'])
    to_user = User.objects.get(username=username)
    FollowUser.objects.filter(from_user=from_user, to_user=to_user).delete()

    if on_flag:
        FollowUser.objects.create(from_user=from_user, to_user=to_user)
        profile = Profile.objects.filter(user=from_user).first()
        if profile:
            avatar_url = '%s/%s' % (
                settings.HOST_URL,
                '%s/%s' % ('media', profile.picture) if profile.picture else static('myapp/img/default_user.png'))
        else:
            avatar_url = static('myapp/img/default_user.png')
        Notification.objects.create(
            user=to_user,
            message='%s started following you' % from_user.username,
            internal_link='{"name": "user", "params":{"username": "%s"}}' % from_user.username,
            image_url=avatar_url
        )

    return JsonResponse({})


@login_required
@csrf_exempt
def follow_playlist(request):
    """
    Follow playlist
    :param request:
    :return:
    """
    user = request.user
    playlist_id = int(request.GET['playlist_id'])
    on_flag = int(request.GET['flag'])

    record = FollowPlaylist.objects.filter(user=user, playlist_id=playlist_id)
    if not record and on_flag:
        # follow the user
        FollowPlaylist.objects.create(user=user, playlist_id=playlist_id)

        Activity.objects.create(
            action='user_follow_playlist',
            playlist_id=playlist_id,
            trigger_user_id=user.id
        )
    elif record and not on_flag:
        # unfollow the user
        record.delete()
        # pass

    return JsonResponse({})


@login_required
@csrf_exempt
def invite_contributor(request):
    """
    Invite contributor to playlist
    :param request:
    :return:
    """
    user = request.user

    email = request.POST['email']

    if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return JsonResponse({'error': "Email address is not correct! "})

    if user.email == email:
        return JsonResponse({'error': "It's your email, you could not invite yourself! "})

    playlist_id = request.POST['playlist_id']
    playlist = Playlist.objects.get(id=playlist_id)
    contributor = Collaborator.objects.filter(email=email, playlist_id=playlist_id).first()
    if not contributor:
        invited_user = User.objects.filter(email=email).first()
        contributor = Collaborator.objects.create(
            email=email,
            user=invited_user,
            playlist_id=playlist_id
        )

        msg = """
Hi

%s invite you to %s playlist.

By accepting the invitation, you can contribute to the playlist, happy listening to the music together!

Please click %s to accept the invitation.

        """ % (request.user.username, playlist.name, contributor.invite_url)

        mail_subject = '%s invite you to %s playlist' % (request.user.username, playlist.name)

        send_mail(
            '[Muusick.tv] %s ' % mail_subject,
            msg,
            'noreply@muusick.tv',
            [email],
            fail_silently=False,
        )

        image_url = '%s/%s' % (
            settings.HOST_URL,
            '%s/%s' % ('media', playlist.image) if playlist.image else static('myapp/img/default_playlist.png'))

        if invited_user:
            Notification.objects.create(
                user=invited_user,
                message=mail_subject,
                internal_link='{"name": "confirmInvite", "params":{"key": "%s"}}' % contributor.invite_key,
                image_url=image_url
            )

        return JsonResponse(ContributorSerializer(contributor, many=False).data)
    else:
        return JsonResponse(
            {'error': "This email is already invited! "}
        )


@login_required
@csrf_exempt
def remove_contributor(request):
    """
    Remove contributor to playlist
    :param request:
    :return:
    """
    playlist_id = int(request.POST['playlist_id'])
    playlist = Playlist.objects.filter(id=playlist_id).first()

    if playlist:
        email = request.POST['email'] if 'email' in request.POST else ''
        username = request.POST['username'] if 'username' in request.POST else ''
        collaborator = None
        if email:
            collaborator = Collaborator.objects.filter(email=email, playlist_id=playlist_id).first()
        if username:
            collaborator = Collaborator.objects.filter(playlist_id=playlist_id, user__username=username).first()

        if collaborator:
            collaborator.delete()

            if not Collaborator.objects.filter(playlist_id=playlist_id).exists():
                playlist.is_collaborative = False
                playlist.save()

            return JsonResponse({
                'delete': 1
            })

    return JsonResponse({
        'delete': 0
    })


@csrf_exempt
@api_view(['GET'])
def get_notifications(request):
    """
    Get user's notifications
    :param request:
    :return:
    """
    if request.user.is_anonymous():
        notifications = []
        new_notification = False
    else:
        notifications = NotificationSerializer(
            Notification.objects.filter(user=request.user)[:15],
            many=True).data

        new_notification = Notification.objects.filter(status=0, user=request.user).exists()

    return Response({
        'notifications': notifications,
        'new_notification': new_notification
    })


@login_required
@csrf_exempt
def get_invitation(request):
    """
    Get user's notifications
    :param request:
    :return:
    """
    invite_key = request.GET['key']
    invite = Collaborator.objects.filter(invite_key=invite_key).first()

    if invite:
        return JsonResponse({
            'invitation': ContributorSerializer(invite, many=False).data,
            'playlist': PlaylistSerializer(invite.playlist, many=False, context={'request': request}).data
        })
    return JsonResponse({
        'invitation': None,
        'playlist': None
    })


@login_required
@csrf_exempt
def accept_invitation(request):
    """
    Get user's notifications
    :param request:
    :return:
    """
    invite_key = request.POST['key']
    invite = Collaborator.objects.filter(invite_key=invite_key).first()

    if invite and invite.status == 0:
        invite.status = 1
        invite.save()

        playlist = invite.playlist
        playlist.is_collaborative = True
        playlist.save()
        image_url = '%s/%s' % (
            settings.HOST_URL,
            '%s/%s' % ('media', playlist.image) if playlist.image else static('myapp/img/default_playlist.png'))

        Notification.objects.create(
            user=playlist.creator,
            message='%s has accepted to join your %s playlist. ' % (request.user.username, playlist.name),
            internal_link='{"name": "playlist", "params":{"slug": "%s"}}' % playlist.slug,
            image_url=image_url
        )

        return JsonResponse({
            'status': invite.status,
            'playlist': PlaylistSerializer(invite.playlist, many=False, context={'request': request}).data
        })

    return JsonResponse({
        'status': 0
    })


@login_required
@csrf_exempt
def view_notification(request):
    """
    View notification
    :param request:
    :return:
    """
    notification = Notification.objects.filter(id=int(request.GET['id'])).first()

    if notification:
        notification.status = 1
        notification.save()

    return JsonResponse({
        'status': 0
    })


@login_required
@csrf_exempt
def get_activities(request):
    """
    Get user's activities
    :param request:
    :return:
    """
    following_users = [f.to_user for f in FollowUser.objects.filter(from_user=request.user)]
    activities = Activity.objects.filter(
        Q(trigger_user__in=following_users)
        |
        Q(trigger_user__isnull=True)
    )[:20]

    defaultPlaylistImage = '/static/myapp/img/default_playlist.png'
    defaultUserImage = '/static/myapp/img/default_user.png'

    results = []
    for a in activities:

        if a.action == 'official_playlist_release':
            if not a.playlist:
                continue
            playlist = Playlist.objects.filter(id=a.playlist_id).first()
            if not playlist:
                continue
            results.append({
                'type': 'p',
                'message': settings.ACTIVITY_ACTIONS[a.action]['template'] % playlist.name,
                'image': playlist.image.url if playlist.image else defaultPlaylistImage,
                'slug': playlist.slug
            })
        elif a.action == 'user_follow_playlist' or a.action == 'user_watch_playlist':
            if not a.playlist or not a.trigger_user:
                continue
            playlist = Playlist.objects.filter(id=a.playlist_id).first()
            if not playlist:
                continue
            trigger_user = User.objects.filter(id=a.trigger_user_id).first()
            if not trigger_user:
                continue
            results.append({
                'type': 'p',
                'message': settings.ACTIVITY_ACTIONS[a.action]['template'] % (trigger_user.username, playlist.name),
                'image': playlist.image.url if playlist.image else defaultPlaylistImage,
                'slug': playlist.slug
            })

    return JsonResponse({
        'activities': results
    })

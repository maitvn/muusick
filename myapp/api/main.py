from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from myapp.serializers import *
from myapp.models import *
from django.contrib.auth.decorators import login_required
import json
from django.core.paginator import Paginator
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.views.decorators.cache import cache_page
from django.core.cache import cache
from .youtube import *

PAGE_SIZE = 30
CACHED_AGE = 60 * 60
SLIDER_LIMIT = 20


@csrf_exempt
@api_view(['GET'])
def initial(request):
    user_serializer = None
    user_playlists = []
    collaboration_playlists = []


    # No cached
    if not  request.user.is_anonymous():
        user_serializer = UserSerializer(
            request.user,
            many=False,
            context={'request': request}
        ).data

        user_playlists = PlaylistSerializer(
            Playlist.objects.filter(creator=request.user).select_related('creator', 'creator__profile'),
            many=True,
            context={'request': request}
        ).data

        _collaboration_playlists = [c.playlist for c in Collaborator.objects.filter(
            user=request.user,
            status=1
        ).select_related('playlist__creator', 'playlist__creator__profile')]

        collaboration_playlists = PlaylistSerializer(
            _collaboration_playlists,
            many=True,
            context={'request': request}
        ).data



    # Cached data
    init_cached_data = cache.get('init_cached_data')
    if not init_cached_data:
        genres = GenreSerializer(Genre.objects.all(), many=True, context={'request': request}).data
        hot_tags = TagSerializer(Tag.objects.filter(is_hot=True), many=True, context={'request': request}).data
        instruments = TagSerializer(Tag.objects.filter(is_instrument=True), many=True, context={'request': request}).data

        feature_videos = VideoSerializer(
            Video.objects.filter(is_feature=True)[:SLIDER_LIMIT],
            many=True,
            context={'request': request}
        ).data

        feature_playlists = PlaylistSerializer(
            Playlist.objects.public().filter(is_public=True, is_feature=True).select_related('creator', 'creator__profile')[:SLIDER_LIMIT],
            many=True,
            context={'request': request}
        ).data

        feature_artists = ArtistSerializer(
            Artist.objects.feature(),
            many=True,
            context={'request': request}
        ).data

        trending_videos = VideoSerializer(
            Video.objects.order_by('-view_count')[:SLIDER_LIMIT],
            many=True,
            context={'request': request}
        ).data

        trending_playlists = PlaylistSerializer(
            Playlist.objects.public().select_related('creator', 'creator__profile').order_by('-view_count')[:SLIDER_LIMIT],
            many=True,
            context={'request': request}
        ).data

        new_added_videos = VideoSerializer(
            Video.objects.order_by('-created_at')[:SLIDER_LIMIT],
            many=True,
            context={'request': request}
        ).data

        new_added_playlists = PlaylistSerializer(
            Playlist.objects.public().select_related('creator', 'creator__profile').order_by('-created_at')[:SLIDER_LIMIT],
            many=True,
            context={'request': request}
        ).data

        personalized_videos = VideoSerializer(
            Video.objects.all()[:SLIDER_LIMIT],
            many=True,
            context={'request': request}
        ).data

        personalized_playlists = PlaylistSerializer(
            Playlist.objects.public().select_related('creator', 'creator__profile')[:SLIDER_LIMIT],
            many=True,
            context={'request': request}
        ).data

        init_cached_data = {
            'genres': genres,
            'instrumentTags': instruments,
            'hotTags': hot_tags,
            'features': {
                'videos': feature_videos,
                'playlists': feature_playlists,
                'artists': feature_artists
            },
            'trending': {
                'videos': trending_videos,
                'playlists': trending_playlists
            },
            'newAdded': {
                'videos': new_added_videos,
                'playlists': new_added_playlists
            },
            'personalized': {
                'videos': personalized_videos,
                'playlists': personalized_playlists
            },
        }

        cache.set('init_cached_data', init_cached_data, CACHED_AGE)

    response = {
        'user': user_serializer,
        'userPlaylists': user_playlists,
        'collaboratePlaylists': collaboration_playlists,
    }
    response.update(init_cached_data)
    return Response(response)


@csrf_exempt
def playing_video_info(request):
    yt_id = request.GET['yt_id'] if 'yt_id' in request.GET else ''
    title = request.GET['title'] if 'title' in request.GET else ''
    params = request.GET['params'] if 'params' in request.GET else ''

    video_object = None
    if params:
        params = json.loads(params)

    if yt_id:
        video_object = Video.objects.filter(yt_id=yt_id).first()

        if not video_object:
            video_object = Video.objects.create(
                yt_id=yt_id,
                title=title,
                # curator_id=request.user.id,
                status=100
            )
        playing_video = VideoDetailSerializer(
            video_object,
            many=False,
            context={'request': request}
        ).data
        playing_video['is_favorite'] = (not request.user.is_anonymous) and FavoriteVideo.objects.filter(
            user=request.user,
            video=video_object).exists()

    if 'artist' in params:
        artist = Artist.objects.filter(id=params['artist']).first()
        if not yt_id:
            video_object = Video.objects.filter(artists__in=[artist]).order_by('?').first()
            playing_video = VideoDetailSerializer(
                video_object,
                many=False,
                context={'request': request}
            ).data
            playing_video['is_favorite'] = (not request.user.is_anonymous) and FavoriteVideo.objects.filter(
                user=request.user,
                video=video_object).exists()

        related_videos = VideoSerializer(
            Video.objects.exclude(id=video_object.id).filter(artists__in=[artist]).order_by('?')[:5],
            many=True,
            context={'request': request}
        ).data

    elif 'genre' in params:
        genre = Genre.objects.filter(id=params['genre']).first()

        if not yt_id:
            video_object = Video.objects.filter(genres__in=[genre]).order_by('?').first()
            playing_video = VideoDetailSerializer(
                video_object,
                many=False,
                context={'request': request}
            ).data
            playing_video['is_favorite'] = (not request.user.is_anonymous) and FavoriteVideo.objects.filter(user=request.user, video=video_object).exists()

        related_videos = VideoSerializer(
            Video.objects.exclude(id=video_object.id).filter(genres__in=[genre]).order_by('?')[:5],
            many=True,
            context={'request': request}
        ).data

    elif 'tag' in params:
        tag = Tag.objects.filter(id=params['tag']).first()
        if not yt_id:
            video_object = Video.objects.filter(tags__in=[tag]).order_by('?').first()
            playing_video = VideoDetailSerializer(
                video_object,
                many=False,
                context={'request': request}
            ).data
            playing_video['is_favorite'] = (not request.user.is_anonymous) and FavoriteVideo.objects.filter(user=request.user, video=video_object).exists()

        related_videos = VideoSerializer(
            Video.objects.exclude(id=video_object.id).filter(tags__in=[tag]).order_by('?')[:5],
            many=True,
            context={'request': request}
        ).data

    elif 'favorite' in params:
        if not video_object:
            video_object = FavoriteVideo.objects.filter(user=request.user).order_by('?').first()
            playing_video = VideoDetailSerializer(
                video_object.video,
                many=False,
                context={'request': request}
            ).data
            playing_video['is_favorite'] = FavoriteVideo.objects.filter(user=request.user, video=video_object.video).exists()

        video_objects = [f.video for f in FavoriteVideo.objects.exclude(video_id=video_object.id).filter(user=request.user).order_by('?')[:5]]
        related_videos = VideoSerializer(
            video_objects,
            many=True,
            context={'request': request}
        ).data

    else:
        related_videos = VideoSerializer(
            Video.objects.order_by('?')[:5],
            many=True,
            context={'request': request}
        ).data
        related_videos = yt_related_video(yt_id, 10)

    return JsonResponse({
        'playingVideo': playing_video,
        'relatedVideos': related_videos,
        'relatedPlaylists': [],
    })


@login_required
@csrf_exempt
def add_to_playlist(request):
    playlist_id = int(request.GET['playlist_id'])
    yt_id = request.GET['yt_id']
    video = Video.objects.get(yt_id=yt_id)
    playlist = Playlist.objects.get(id=playlist_id)

    instance = PlaylistVideo.objects.filter(playlist=playlist, video=video)
    if not instance:
        PlaylistVideo.objects.create(playlist=playlist, video=video, added_by=request.user)

    return JsonResponse({
    })


@login_required
@csrf_exempt
def add_to_favorite(request):
    yt_id = request.GET['yt_id']
    flag = request.GET['flag']
    video = Video.objects.get(yt_id=yt_id)

    if flag == 'true':
        if not FavoriteVideo.objects.filter(video=video, user=request.user).exists():
            FavoriteVideo.objects.create(video=video, user=request.user)

    else:
        if FavoriteVideo.objects.filter(video=video, user=request.user).exists():
            FavoriteVideo.objects.filter(video=video, user=request.user).delete()

    return JsonResponse({
    })


@api_view(['GET'])
@csrf_exempt
@cache_page(CACHED_AGE)
def browse_genre(request, type, id, order):
    """
    Browse genre or tag
    2017/03/09
    """
    if order == 'random':
        order = '?'
    elif order == 'time':
        order = '-created_at'
    else:
        order = '-view_count'

    detail = None
    if type == 'genre':
        genre = Genre.objects.get(pk=id)
        detail = GenreSerializer(genre, many=False, context={'request': request})
        video_serializer = VideoSerializer(
            Video.objects.filter(genres=genre).order_by(order)[:20],
            many=True, context={'request': request})
        playlist_serializer = PlaylistSerializer(
            Playlist.objects.public().filter(genres=genre).select_related('creator', 'creator__profile').order_by(order)[:20],
            many=True, context={'request': request})

        if genre.country:
            artists_serializer = ArtistSerializer(
                Artist.objects.filter(country=genre.country).order_by('?')[:20],
                many=True,
                context={'request': request}
            )
        else:
            artists_serializer = ArtistSerializer(
                Artist.objects.filter(genre=genre).order_by('?')[:15],
                many=True,
                context={'request': request}
            )
    elif type == 'tag':
        tag = Tag.objects.get(pk=id)
        detail = TagSerializer(tag, many=False, context={'request': request})
        video_serializer = VideoSerializer(
            Video.objects.filter(tags=tag).order_by(order)[:20],
            many=True, context={'request': request})
        playlist_serializer = PlaylistSerializer(
            Playlist.objects.public().select_related('creator', 'creator__profile').filter(tags=tag).order_by(order)[:20],
            many=True, context={'request': request})
        artists_serializer = ArtistSerializer(
            Artist.objects.filter(tag=tag).order_by('?')[:20],
            many=True,
            context={'request': request}
        )
    return Response({
        'detail': detail.data,
        'videos': video_serializer.data,
        'playlists': playlist_serializer.data,
        'artists': artists_serializer.data,
    })


@api_view(['GET'])
@cache_page(CACHED_AGE)
@csrf_exempt
def get_playlist_list(request):
    """
    Get playlist list
    :param request:
    :return:
    """
    playlist_serializer = PlaylistSerializer(
        Playlist.objects.public().order_by('-view_count', '-song_count').select_related('creator', 'creator__profile')[:100],
        many=True, context={'request': request})

    return Response({
        'playlists': playlist_serializer.data,
    })


@csrf_exempt
def get_favorites(request):
    if request.user.is_anonymous():
        favorite_videos = []
    else:
        videos = [f.video for f in FavoriteVideo.objects.filter(user=request.user)[:40]]
        favorite_videos = VideoSerializer(videos, many=True, context={'request': request}).data

    return JsonResponse({
        'videos': favorite_videos
    })


@api_view(['GET'])
@csrf_exempt
def get_following_playlists(request):
    if request.user.is_anonymous():
        follow_playlists = []
    else:
        playlists = [f.playlist for f in FollowPlaylist.objects.filter(
            playlist__is_public=True,
            user=request.user
        ).select_related('playlist__creator', 'playlist__creator__profile')]

        follow_playlists = PlaylistSerializer(playlists, many=True, context={'request': request}).data
    return Response({
        'playlists': follow_playlists
    })


@api_view(['GET'])
@csrf_exempt
def get_history(request):
    if request.user.is_anonymous():
        history_videos = []
        history_playlists = []
    else:
        videos = [f.video for f in HistoryVideo.objects.filter(user=request.user).select_related('video')[:20]]

        history_videos = VideoSerializer(
            videos,
            many=True,
            context={'request': request}
        ).data

        playlists = [f.playlist for f in HistoryPlaylist.objects.filter(
            playlist__is_public=True,
            user=request.user
        ).select_related('playlist__creator', 'playlist__creator__profile')[:20]]

        history_playlists = PlaylistSerializer(playlists, many=True, context={'request': request}).data
    return Response({
        'videos': history_videos,
        'playlists': history_playlists
    })


@cache_page(CACHED_AGE)
@api_view(['GET'])
@csrf_exempt
def get_list(request):
    param = request.GET['param']
    page = int(request.GET['page'])

    playlist = {}
    title = ''
    total_page = 1

    p_min, p_max = (page - 1) * PAGE_SIZE, (page - 1) * PAGE_SIZE + PAGE_SIZE

    if request.GET['type'] == 'artist':
        if param == 'feature':
            query = Artist.objects.filter(is_feature=True).order_by('-score')
            artist = ArtistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Feature'

        elif param == 'trending':
            query = Artist.objects.order_by('-score')
            artist = ArtistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Trending'

        elif 'genre' in param:
            (genre, id) = param.split('=')
            genre_obj = Genre.objects.get(pk=id)
            query = Artist.objects.filter(genre=genre_obj).order_by('-score')
            artist = ArtistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = genre_obj.name

        elif 'tag' in param:
            (tag, id) = param.split('=')
            tag_obj = Tag.objects.get(pk=id)
            query = Artist.objects.filter(tag=tag_obj).order_by('-score')
            artist = ArtistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = tag_obj.name
        else:
            return JsonResponse({})

        p = Paginator(query, PAGE_SIZE)
        return JsonResponse({
            'artists': artist.data,
            'title': title,
            'total_page': p.num_pages
        })

    elif request.GET['type'] == 'playlist':
        if param == 'feature':
            query = Playlist.objects.public().select_related('creator', 'creator__profile').filter(is_feature=True)
            playlist = PlaylistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Feature'

        elif param == 'trending':
            query = Playlist.objects.public().select_related('creator', 'creator__profile').filter(is_feature=True).order_by('-view_count')
            playlist = PlaylistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Trending'

        elif 'genre' in param:
            (genre, id) = param.split('=')
            genre_obj = Genre.objects.get(pk=id)
            query = Playlist.objects.public().select_related('creator', 'creator__profile').filter(genres=genre_obj).order_by('-view_count')
            playlist = PlaylistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = genre_obj.name

        elif 'tag' in param:
            (genre, id) = param.split('=')
            tag_obj = Tag.objects.get(pk=id)
            query = Playlist.objects.public().select_related('creator', 'creator__profile').filter(tags=tag_obj).order_by('-view_count')
            playlist = PlaylistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = tag_obj.name
        else:
            return JsonResponse({})

        p = Paginator(query, PAGE_SIZE)
        return JsonResponse({
            'playlists': playlist.data,
            'title': title,
            'total_page': p.num_pages
        })

    elif request.GET['type'] == 'video':
        videos = {}
        title = ''
        query = None
        if param == 'feature':
            query = Video.objects.filter(is_feature=True)
            videos = VideoSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Feature'

        elif param == 'trending':
            query = Video.objects.filter(is_feature=True).order_by('-view_count')
            videos = VideoSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Trending'

        elif 'genre' in param:
            (genre, id) = param.split('=')
            genre_obj = Genre.objects.get(pk=id)
            query = Video.objects.filter(genres=genre_obj).order_by('-view_count')
            videos = VideoSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = genre_obj.name

        elif 'tag' in param:
            (genre, id) = param.split('=')
            tag_obj = Tag.objects.get(pk=id)
            query = Video.objects.filter(tags=tag_obj).order_by('-view_count')
            videos = VideoSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = tag_obj.name

        else:
            return JsonResponse({})

        p = Paginator(query, PAGE_SIZE)
        return Response({
            'videos': videos.data,
            'title': title,
            'total_page': p.num_pages
        })

    return Response({})


@csrf_exempt
def add_history_video(request):
    video = Video.objects.filter(yt_id=request.POST['yt_id']).first()
    if not video:
        return JsonResponse({
            'error': 'yt_id: %s' % request.POST['yt_id']
        })
    video.view_count += 1
    video.save()
    history = HistoryVideo.objects.filter(user=request.user, video=video).first()
    if not history:
        history = HistoryVideo.objects.create(user=request.user, video=video)

    history.save()
    return JsonResponse({
        'success': '1'
    })


@csrf_exempt
def add_history_playlist(request):
    if request.user.is_anonymous:
        return JsonResponse({
            'error': 'not login'
        })

    playlist = Playlist.objects.filter(slug=request.POST['slug']).first()
    if not playlist:
        return JsonResponse({
            'error': 'slug: %s' % request.POST['slug']
        })
    playlist.view_count += 1
    playlist.save()
    history = HistoryPlaylist.objects.filter(user=request.user, playlist=playlist).first()
    if not history:
        history = HistoryPlaylist.objects.create(user=request.user, playlist=playlist)

    history.save()
    playlist.is_public and Activity.objects.create(
        action='user_watch_playlist',
        trigger_user_id=request.user.id,
        playlist_id=playlist.id
    )
    return JsonResponse({
        'success': '1'
    })


@csrf_exempt
def upload_profile_picture(request):
    user = request.user
    profile = Profile.objects.filter(pk=user.id).first()
    if not profile:
        profile = Profile(user=user)
        profile.save()

    if request.method == 'POST':
        form = FileUploadForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            # handle_uploaded_file(request.FILES['file_source'])
            f = request.FILES['file_source']
            filename = '%s%d.jpg' % ('/media/users/', user.id)
            with open(SITE_ROOT + filename, 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)

            image = Image.open(SITE_ROOT + filename)
            width, height = image.size
            size = min(width, height)
            left = (width - size) / 2
            top = (height - size) / 2
            right = (width + size) / 2
            bottom = (height + size) / 2

            croped = image.crop((left, top, right, bottom))
            croped.save(SITE_ROOT + filename)

            profile.picture = 'users/%d.jpg' % user.id
            profile.save()
        else:
            pass
    return JsonResponse({
        'code': 100,
        'msg': 'success',
        'avatar': filename
    })


@csrf_exempt
def get_detail_artist_info(request):
    artist_obj = Artist.objects.filter(name=request.GET['name']).first()
    if artist_obj:
        artist_info = ArtistDetailSerializer(
            artist_obj,
            many=False,
            context={'request': request}
        )

        videos = VideoSerializer(
            Video.objects.filter(artists__in=[artist_obj]),
            many=True,
            context={'request': request}
        )

        return JsonResponse({
            'artist': artist_info.data,
            'videos': videos.data
        })

    return JsonResponse({
    })

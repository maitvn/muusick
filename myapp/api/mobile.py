from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from myapp.serializers import *
from myapp.models import *
import json
from django.core.paginator import Paginator
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.views.decorators.cache import cache_page
from .youtube import *
from django.core.exceptions import ObjectDoesNotExist
from .youtube import yt_search
from django.contrib.auth.models import User

PAGE_SIZE = 30
CACHED_AGE = 60 * 60
SLIDER_LIMIT = 20


@csrf_exempt
@api_view(['GET'])
@cache_page(CACHED_AGE)
def category(request):
    genres = GenreSerializer(Genre.objects.all(), many=True, context={'request': request}).data
    hot_tags = TagSerializer(Tag.objects.filter(is_hot=True), many=True, context={'request': request}).data
    instruments = TagSerializer(Tag.objects.filter(is_instrument=True), many=True, context={'request': request}).data
    return Response({
        'genres': genres,
        'instrumentTags': instruments,
        'hotTags': hot_tags,
    })


@csrf_exempt
@api_view(['GET'])
@cache_page(CACHED_AGE)
def home(request):

    feature_videos = VideoSerializer(
        Video.objects.filter(is_feature=True).order_by('-display_order')[:SLIDER_LIMIT],
        many=True,
        context={'request': request}
    ).data

    feature_playlists = PlaylistSerializer(
        Playlist.objects.public().filter(
            is_public=True,
            is_feature=True
        ).select_related('creator', 'creator__profile').order_by('?')[:SLIDER_LIMIT],
        many=True,
        context={'request': request}
    ).data

    mood_playlists = PlaylistSerializer(
        Playlist.objects.public().filter(genres=Genre.objects.get(pk=settings.MOOD_GENRE_ID)).select_related(
            'creator',
            'creator__profile'
        ).order_by('?')[:SLIDER_LIMIT],
        many=True,
        context={'request': request}
    ).data

    new_playlists = PlaylistSerializer(
        Playlist.objects.public().select_related(
            'creator',
            'creator__profile'
        ).order_by('-created_at')[:SLIDER_LIMIT],
        many=True,
        context={'request': request}
    ).data

    top_chart_playlists = PlaylistSerializer(
        Playlist.objects.public().filter(is_top_chart=True).select_related(
            'creator',
            'creator__profile').order_by('-view_count')[:SLIDER_LIMIT],
        many=True,
        context={'request': request}
    ).data

    return Response({
        'topChartPlaylists': top_chart_playlists,
        'featurePlaylists': feature_playlists,
        'newPlaylists': new_playlists,
        'moodPlaylists': mood_playlists,
        'featureVideos': feature_videos,
    })


@csrf_exempt
def playing_video_info(request):
    yt_id = request.GET['yt_id'] if 'yt_id' in request.GET else ''
    title = request.GET['title'] if 'title' in request.GET else ''
    params = request.GET['params'] if 'params' in request.GET else ''

    video_object = None
    if params:
        params = json.loads(params)

    if yt_id:
        video_object = Video.objects.filter(yt_id=yt_id).first()

        if not video_object:
            video_object = Video.objects.create(
                yt_id=yt_id,
                title=title,
                # curator_id=request.user.id,
                status=100
            )
        playing_video = VideoDetailSerializer(
            video_object,
            many=False,
            context={'request': request}
        ).data
        playing_video['is_favorite'] = (not request.user.is_anonymous) and FavoriteVideo.objects.filter(
            user=request.user,
            video=video_object).exists()

    if 'artist' in params:
        artist = Artist.objects.filter(id=params['artist']).first()
        if not yt_id:
            video_object = Video.objects.filter(artists__in=[artist]).order_by('?').first()
            playing_video = VideoDetailSerializer(
                video_object,
                many=False,
                context={'request': request}
            ).data
            playing_video['is_favorite'] = (not request.user.is_anonymous) and FavoriteVideo.objects.filter(
                user=request.user,
                video=video_object).exists()

        related_videos = VideoSerializer(
            Video.objects.exclude(id=video_object.id).filter(artists__in=[artist]).order_by('?')[:5],
            many=True,
            context={'request': request}
        ).data

    elif 'genre' in params:
        genre = Genre.objects.filter(id=params['genre']).first()

        if not yt_id:
            video_object = Video.objects.filter(genres__in=[genre]).order_by('?').first()
            playing_video = VideoDetailSerializer(
                video_object,
                many=False,
                context={'request': request}
            ).data
            playing_video['is_favorite'] = (not request.user.is_anonymous) and FavoriteVideo.objects.filter(user=request.user, video=video_object).exists()

        related_videos = VideoSerializer(
            Video.objects.exclude(id=video_object.id).filter(genres__in=[genre]).order_by('?')[:5],
            many=True,
            context={'request': request}
        ).data

    elif 'tag' in params:
        tag = Tag.objects.filter(id=params['tag']).first()
        if not yt_id:
            video_object = Video.objects.filter(tags__in=[tag]).order_by('?').first()
            playing_video = VideoDetailSerializer(
                video_object,
                many=False,
                context={'request': request}
            ).data
            playing_video['is_favorite'] = (not request.user.is_anonymous) and FavoriteVideo.objects.filter(user=request.user, video=video_object).exists()

        related_videos = VideoSerializer(
            Video.objects.exclude(id=video_object.id).filter(tags__in=[tag]).order_by('?')[:5],
            many=True,
            context={'request': request}
        ).data

    elif 'favorite' in params:
        if not video_object:
            video_object = FavoriteVideo.objects.filter(user=request.user).order_by('?').first()
            playing_video = VideoDetailSerializer(
                video_object.video,
                many=False,
                context={'request': request}
            ).data
            playing_video['is_favorite'] = FavoriteVideo.objects.filter(user=request.user, video=video_object.video).exists()

        video_objects = [f.video for f in FavoriteVideo.objects.exclude(video_id=video_object.id).filter(user=request.user).order_by('?')[:5]]
        related_videos = VideoSerializer(
            video_objects,
            many=True,
            context={'request': request}
        ).data

    else:
        related_videos = VideoSerializer(
            Video.objects.order_by('?')[:5],
            many=True,
            context={'request': request}
        ).data
        related_videos = yt_related_video(yt_id, 10)

    return JsonResponse({
        'playingVideo': playing_video,
        'relatedVideos': related_videos,
        'relatedPlaylists': [],
    })


@api_view(['GET'])
@csrf_exempt
@cache_page(CACHED_AGE)
def browse(request):
    """
    Browse genre or tag
    2017/03/09
    """
    type = request.GET['type']
    id = request.GET['id']
    order = '-view_count'
    if type == 'genre':
        genre = Genre.objects.get(pk=id)
        detail = GenreSerializer(genre, many=False, context={'request': request})
        video_serializer = VideoSerializer(
            Video.objects.filter(genres=genre).order_by(order)[:20],
            many=True, context={'request': request})
        playlist_serializer = PlaylistSerializer(
            Playlist.objects.public().filter(genres=genre).select_related('creator', 'creator__profile').order_by(order)[:20],
            many=True, context={'request': request})

        if genre.country:
            artists_serializer = ArtistSerializer(
                Artist.objects.filter(country=genre.country).order_by('?')[:20],
                many=True,
                context={'request': request}
            )
        else:
            artists_serializer = ArtistSerializer(
                Artist.objects.filter(genre=genre).order_by('?')[:15],
                many=True,
                context={'request': request}
            )
    elif type == 'tag':
        tag = Tag.objects.get(pk=id)
        detail = TagSerializer(tag, many=False, context={'request': request})
        video_serializer = VideoSerializer(
            Video.objects.filter(tags=tag).order_by(order)[:20],
            many=True, context={'request': request})
        playlist_serializer = PlaylistSerializer(
            Playlist.objects.public().select_related('creator', 'creator__profile').filter(tags=tag).order_by(order)[:20],
            many=True, context={'request': request})
        artists_serializer = ArtistSerializer(
            Artist.objects.filter(tag=tag).order_by('?')[:20],
            many=True,
            context={'request': request}
        )
    return Response({
        'detail': detail.data,
        'videos': video_serializer.data,
        'playlists': playlist_serializer.data,
        'artists': artists_serializer.data,
    })


@api_view(['GET'])
@cache_page(CACHED_AGE)
@csrf_exempt
def all_playlist(request):
    all_playlists = PlaylistSerializer(
        Playlist.objects.public().order_by(
            '-view_count',
            '-song_count'
        ).select_related('creator', 'creator__profile')[:50],
        many=True,
        context={'request': request}).data

    return Response({
        'playlists': all_playlists,
    })


@cache_page(CACHED_AGE)
@api_view(['GET'])
@csrf_exempt
def get_list(request):
    param = request.GET['param']
    page = int(request.GET['page'])

    playlist = {}
    title = ''
    total_page = 1

    p_min, p_max = (page - 1) * PAGE_SIZE, (page - 1) * PAGE_SIZE + PAGE_SIZE

    if request.GET['type'] == 'artist':
        if param == 'feature':
            query = Artist.objects.filter(is_feature=True).order_by('-score')
            artist = ArtistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Feature'

        elif param == 'trending':
            query = Artist.objects.order_by('-score')
            artist = ArtistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Trending'

        elif 'genre' in param:
            (genre, id) = param.split('=')
            genre_obj = Genre.objects.get(pk=id)
            query = Artist.objects.filter(genre=genre_obj).order_by('-score')
            artist = ArtistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = genre_obj.name

        elif 'tag' in param:
            (tag, id) = param.split('=')
            tag_obj = Tag.objects.get(pk=id)
            query = Artist.objects.filter(tag=tag_obj).order_by('-score')
            artist = ArtistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = tag_obj.name
        else:
            return JsonResponse({})

        p = Paginator(query, PAGE_SIZE)
        return JsonResponse({
            'artists': artist.data,
            'title': title,
            'total_page': p.num_pages
        })

    elif request.GET['type'] == 'playlist':
        if param == 'feature':
            query = Playlist.objects.public().select_related('creator', 'creator__profile').filter(is_feature=True)
            playlist = PlaylistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Feature'

        elif param == 'trending':
            query = Playlist.objects.public().select_related('creator', 'creator__profile').filter(is_feature=True).order_by('-view_count')
            playlist = PlaylistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Trending'

        elif 'genre' in param:
            (genre, id) = param.split('=')
            genre_obj = Genre.objects.get(pk=id)
            query = Playlist.objects.public().select_related('creator', 'creator__profile').filter(genres=genre_obj).order_by('-view_count')
            playlist = PlaylistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = genre_obj.name

        elif 'tag' in param:
            (genre, id) = param.split('=')
            tag_obj = Tag.objects.get(pk=id)
            query = Playlist.objects.public().select_related('creator', 'creator__profile').filter(tags=tag_obj).order_by('-view_count')
            playlist = PlaylistSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = tag_obj.name
        else:
            return JsonResponse({})

        p = Paginator(query, PAGE_SIZE)
        return JsonResponse({
            'playlists': playlist.data,
            'title': title,
            'total_page': p.num_pages
        })

    elif request.GET['type'] == 'video':
        videos = {}
        title = ''
        query = None
        if param == 'feature':
            query = Video.objects.filter(is_feature=True)
            videos = VideoSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Feature'

        elif param == 'trending':
            query = Video.objects.filter(is_feature=True).order_by('-view_count')
            videos = VideoSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = 'Trending'

        elif 'genre' in param:
            (genre, id) = param.split('=')
            genre_obj = Genre.objects.get(pk=id)
            query = Video.objects.filter(genres=genre_obj).order_by('-view_count')
            videos = VideoSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = genre_obj.name

        elif 'tag' in param:
            (genre, id) = param.split('=')
            tag_obj = Tag.objects.get(pk=id)
            query = Video.objects.filter(tags=tag_obj).order_by('-view_count')
            videos = VideoSerializer(
                query[p_min: p_max],
                many=True,
                context={'request': request}
            )
            title = tag_obj.name

        else:
            return JsonResponse({})

        p = Paginator(query, PAGE_SIZE)
        return Response({
            'videos': videos.data,
            'title': title,
            'total_page': p.num_pages
        })

    return Response({})


@csrf_exempt
@api_view(['GET'])
@cache_page(CACHED_AGE)
def playlist_data(request):
    """
    Get playlist detail information (for playing)
    :param request:
    :return:
    """
    try:
        playlist = Playlist.objects.get(slug=request.GET['slug'])
    except ObjectDoesNotExist:
        return Response({
            'data': {},
        })

    playlist_data = PlaylistDetailSerializer(
        playlist,
        many=False,
        context={'request': request}).data

    videos = VideoDetailSerializer(
        playlist.videos.all().prefetch_related('tags', 'artists', 'genres'),
        many=True,
        context={'request': request}
    ).data

    return Response({
        'playlist': playlist_data,
        'videos': videos,
    })


@csrf_exempt
@api_view(['GET'])
def search(request):
    keyword = request.GET['q']
    yt_videos = yt_search(keyword, 20)

    videos = VideoSerializer(
        Video.objects.filter(title__icontains=keyword)[:6],
        many=True,
        context={'request': request}
    )

    playlists = PlaylistSerializer(
        Playlist.objects.public().filter(name__icontains=keyword).select_related('creator', 'creator__profile')[:6],
        many=True,
        context={'request': request}
    )

    tags = TagSerializer(
        Tag.objects.filter(name__icontains=keyword)[:6],
        many=True,
        context={'request': request}
    )

    artists = ArtistSerializer(
        Artist.objects.filter(name__icontains=keyword)[:6],
        many=True,
        context={'request': request}
    )
    #
    # users = UserDetailSerializer(
    #     User.objects.filter(username__icontains=keyword)[:6],
    #     many=True,
    #     context={'request': request}
    # )

    return Response({
        'query': keyword,
        'results': {
            # 'users': users.data or None,
            'tags': tags.data or [],
            'artists': artists.data or [],
            'playlists': playlists.data or [],
            'videos': videos.data or [],
            'ytVideos': yt_videos or []
        }
    })


@csrf_exempt
def get_related_videos(request):
    yt_id = request.GET['yt_id'] if 'yt_id' in request.GET else ''
    title = request.GET['title'] if 'title' in request.GET else ''
    related_videos = yt_related_video(yt_id, 10)
    return JsonResponse({
        'relatedVideos': related_videos,
    })

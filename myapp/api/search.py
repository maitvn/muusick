from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from myapp.models import *
from myapp.serializers import *
from .youtube import yt_search
from rest_framework.response import Response
from rest_framework.decorators import api_view



@csrf_exempt
@api_view(['GET'])
def search_suggest(request):
    """
    Main search API
    :param request:
    :return:
    """
    type = request.GET['type']
    keyword = request.GET['keyword']
    is_yt_search = int(request.GET['yt_search'])
    yt_videos = []

    if type == 'all':
        if is_yt_search:
            yt_videos = yt_search(keyword, 10)

        videos = VideoSerializer(
            Video.objects.filter(title__icontains=keyword)[:6],
            many=True,
            context={'request': request}
        )

        playlists = PlaylistSerializer(
            Playlist.objects.public().filter(name__icontains=keyword).select_related('creator', 'creator__profile')[:6],
            many=True,
            context={'request': request}
        )

        tags = TagSerializer(
            Tag.objects.filter(name__icontains=keyword)[:6],
            many=True,
            context={'request': request}
        )

        artists = ArtistSerializer(
            Artist.objects.filter(name__icontains=keyword)[:6],
            many=True,
            context={'request': request}
        )

        users = UserDetailSerializer(
            User.objects.filter(username__icontains=keyword)[:6],
            many=True,
            context={'request': request}
        )

        return Response({
            'query': keyword,
            'results': {
                'users': users.data or None,
                'tags': tags.data or None,
                'artists': artists.data or None,
                'playlists': playlists.data or None,
                'videos': videos.data or None,
                'ytVideos': yt_videos or None
            }
        })

    if type == 'video':
        yt_videos = yt_search(keyword, 40)
        videos = VideoSerializer(
            Video.objects.filter(title__icontains=keyword)[:40],
            many=True,
            context={'request': request}
        )

        return Response({
            'query': keyword,
            'results': {
                'videos': videos.data or None,
                'ytVideos': yt_videos or None
            }
        })

    if type == 'artist':
        artists = ArtistSerializer(
            Artist.objects.filter(name__icontains=keyword)[:40],
            many=True,
            context={'request': request}
        ).data

        return Response({
            'query': keyword,
            'results': {
                'artists': artists,
            }
        })
    if type == 'playlist':
        playlists = PlaylistSerializer(
            Playlist.objects.public().filter(
                name__icontains=keyword
            ).select_related('creator', 'creator__profile')[:40],
            many=True,
            context={'request': request}
        ).data

        return Response({
            'query': keyword,
            'results': {
                'playlists': playlists,
            }
        })



@csrf_exempt
def search_users(request):
    keyword = request.GET['keyword']
    users = UserDetailSerializer(
        User.objects.exclude(id=request.user.id).filter(username__icontains=keyword)[:10],
        many=True,
        context={'request': request})

    return JsonResponse({
        'query': keyword,
        'users': users.data,
    })



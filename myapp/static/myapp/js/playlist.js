// This is the event hub we'll use in every
// component to communicate between them.
var eventHub = new Vue()

var vm = new Vue({
  el: '#container',
  data: {
    player: null,
    playingIdx: -1,
    playOrder: [],
    videos: window.videos,
    shuffleMode: false,
    repeatMode: false,
    showPlayer: false,
  },
  mounted: function () {
    // request permission on page load
    if (!window.Notification) {
      return
    }
    if (window.Notification.permission !== 'granted') {
      window.Notification.requestPermission()
    }

//    $('#videos').perfectScrollbar();
//    Ps.initialize(document.getElementById('videos'), {
//        suppressScrollX: true,
//        wheelSpeed: 2,
//        wheelPropagation: false,
//        minScrollbarLength: 20
//      })
  },
  created: function () {
    eventHub.$on('player:ready', this.playerReady)
  },
  methods: {
    initPlayer: function () {
        var self = this;
        var ytId = this.playingIdx >= 0 ? this.videos[this.playOrder[this.playingIdx]].yt_id : ''
        this.player = new YT.Player('player', {
            height: '390',
            width: '640',
            videoId: ytId,
            autoplay: 0,
            playsinline:1,
            fs:0,
            events: {
              'onReady': function (event) {
                event.target.playVideo();
              },
              'onStateChange': function (event) {
                if(event.data === YT.PlayerState.ENDED) {
                    self.next(true, true)
                }
//                console.log('>>>', event.data)
//                console.log('YT.PlayerState.PLAYING', YT.PlayerState.PLAYING)
//                console.log('YT.PlayerState.ENDED', YT.PlayerState.ENDED)
//                console.log('YT.PlayerState.UNSTARTED', YT.PlayerState.UNSTARTED)
//                console.log('YT.PlayerState.PAUSED', YT.PlayerState.PAUSED)
//                console.log('YT.PlayerState.BUFFERING', YT.PlayerState.BUFFERING)
//                console.log('YT.PlayerState.CUED', YT.PlayerState.CUED)
              },
              'onError': function (event) {
                this.playingIdx >=0 && self.next(true, true)
              }
            }
        });
    },
    playerReady: function () {
        this.playOrder = this._range(this.videos.length)
        this.initPlayer()
    },
    play: function (yt_id) {
        this.player.loadVideoById(yt_id, 0)
    },
    playShuffle: function () {
        this.showPlayer = true
        this.setShuffle()
        this.playingIdx = 0
        this.play(this.videos[this.playOrder[this.playingIdx]].yt_id)
    },
    playNormal: function () {
        this.showPlayer = true
        this.playingIdx = 0
        this.play(this.videos[this.playOrder[this.playingIdx]].yt_id)
    },
    playByIdx: function (idx) {
        this.showPlayer = true
        this.playingIdx = idx
        this.play(this.videos[this.playOrder[this.playingIdx]].yt_id)
    },
    next: function (flag, notify) {
        if (flag) {
            this.playingIdx = this.playingIdx === this.videos.length - 1 ? 0 : this.playingIdx + 1
        } else {
            this.playingIdx = this.playingIdx === 0 ? this.videos.length - 1 : this.playingIdx - 1
        }
        this.play(this.videos[this.playingIdx].yt_id)
        notify && this.notify(this.videos[this.playingIdx].yt_id, this.videos[this.playingIdx].title)
    },
    setShuffle: function () {
        this.shuffleMode = !this.shuffleMode
        if (this.shuffleMode) {
            this._shuffle(this.playOrder)
        }
    },
    setRepeat: function () {
        this.repeatMode = !this.repeatMode
    },
    _shuffle: function (a) {
      var j, x, i
      for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i)
        x = a[i - 1]
        a[i - 1] = a[j]
        a[j] = x
      }
    },
    _range: function (n) {
        var arr = []
        for (var i = 0; i < n; i++) {
            arr.push(i)
        }
        return arr
    },
    notify: function (ytId, message) {
        if (window.Notification.permission === 'granted') {
          var notification = new window.Notification('Playing', {
            icon: 'https://img.youtube.com/vi/' + ytId + '/default.jpg',
            body: message
          })

          notification.onclick = function () {
            this.close()
          }

          setTimeout(() => {
            notification.close()
          }, 5000)
        }
    },
  }
})



// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function onYouTubeIframeAPIReady() {
    eventHub.$emit('player:ready')
}

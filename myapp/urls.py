from django.conf.urls import url

from . import views
from api import playlist, search, user, main, youtube, mobile
from extension import views as extension


urlpatterns = [
    url(r'^$', views.index),
    url(r'^landing$', views.landing),
    url(r'^discover$', views.discover),
    url(r'^confirm_invite_playlist/(?P<key>.*)$$', views.confirm_invite_playlist, name='invite_url'),
    url(r'^public/pl/(?P<slug>.*)$', views.playlist, name='share_url'),

    # Main API
    url(r'^api/initial', main.initial, name='api_initial'),

    # Playing related APIs
    url(r'^api/playing_video_info', main.playing_video_info),
    url(r'^api/get_playlist_data$', playlist.get_playlist_data, name='api_get_playlist_data'),
    url(r'^api/get_playlist_private_data', playlist.get_playlist_private_data),

    # Playlist APIs
    url(r'^api/add_to_playlist$', main.add_to_playlist),
    url(r'^api/add_to_favorite', main.add_to_favorite),
    url(r'^api/delete_playlist$', playlist.delete_playlist),
    url(r'^api/publish_playlist$', playlist.publish_playlist),
    url(r'^api/remove_from_playlist$', playlist.remove_from_playlist),
    url(r'^api/create_playlist$', playlist.create_playlist),
    url(r'^api/delete_playlist$', playlist.delete_playlist),
    url(r'^api/update_playlist$', playlist.update_playlist),
    url(r'^api/follow_playlist$', user.follow_playlist),
    url(r'^api/invite_contributor', user.invite_contributor),
    url(r'^api/remove_contributor', user.remove_contributor),
    url(r'^api/accept_invitation', user.accept_invitation),

    # Artists API
    url(r'^api/get_detail_artist_info', main.get_detail_artist_info),

    # Search, Browse APIs
    url(r'^api/search_users', search.search_users),
    url(r'^api/search_suggest$', search.search_suggest),
    url(r'^api/browse/(?P<type>[a-z]+)/(?P<id>[0-9]+)/(?P<order>.*)', main.browse_genre, name='browse'),
    url(r'^api/get_list', main.get_list),
    url(r'^api/get_playlist_list', main.get_playlist_list, name='api_get_playlist_list'),

    url(r'^api/youtube/search', youtube.search),


    # History API
    url(r'^api/get_favorites$', main.get_favorites),
    url(r'^api/add_history_video$', main.add_history_video),
    url(r'^api/add_history_playlist$', main.add_history_playlist),
    url(r'^api/get_following_playlists$', main.get_following_playlists),
    url(r'^api/get_history', main.get_history),


    # User related api
    url(r'^api/get_user_profile$', user.get_user_profile),
    url(r'^api/update_profile$', user.update_profile),
    url(r'^api/follow_user$', user.follow_user),
    url(r'^api/get_notifications', user.get_notifications),
    url(r'^api/get_invitation', user.get_invitation),
    url(r'^api/view_notification', user.view_notification),
    url(r'^api/get_activities', user.get_activities),


    # Bookmark's apis
    url(r'^api/ext/init', extension.init),
    url(r'^api/ext/lists', extension.lists),
    url(r'^api/ext/create_list', extension.create_list),
    url(r'^api/ext/browse_lists', extension.browse_lists),
    url(r'^api/ext/add_2_list', extension.add_2_list),
    url(r'^api/ext/get_video$', extension.get_video),
    url(r'^api/ext/update_artists', extension.update_artists),
    url(r'^api/ext/update_tags', extension.update_tags),
    url(r'^api/ext/update_genres', extension.update_genres),
    url(r'^api/ext/get_tag_suggestions', extension.get_tag_suggestions),
    url(r'^api/ext/get_artist_suggestions', extension.get_artist_suggestions),

    # Mobile apps API
    url(r'^api/mobile/v1/home', mobile.home),
    url(r'^api/mobile/v1/category', mobile.category),
    url(r'^api/mobile/v1/browse', mobile.browse),
    url(r'^api/mobile/v1/all_playlist', mobile.all_playlist),
    url(r'^api/mobile/v1/get_list', mobile.get_list),
    url(r'^api/mobile/v1/playlist_data', mobile.playlist_data),
    url(r'^api/mobile/v1/search', mobile.search),
    url(r'^api/mobile/v1/get_related_videos', mobile.get_related_videos),
]
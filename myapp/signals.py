from allauth.account.signals import user_logged_in,user_signed_up
from django.dispatch import receiver
from allauth.account.signals import user_logged_in,user_signed_up

from allauth.socialaccount.signals import pre_social_login, social_account_added, social_account_removed

from myapp.models import Profile

GRAPH_API_URL = 'https://graph.facebook.com/'


@receiver(user_logged_in)
def user_login(sender, **kwargs):
  print 'User has been login'
  # print kwargs['request']
  # print kwargs['user']


@receiver(user_signed_up)
def user_signed_up(sender, **kwargs):
  if 'sociallogin' in kwargs:
    account = kwargs['sociallogin'].account

    if account.provider == 'facebook' and not Profile.objects.filter(user_id=account.user_id).exists():
      avatar_url = GRAPH_API_URL + '/%s/picture?type=square&height=300&width=300&return_ssl_resources=1' % account.uid
      profile = Profile(user_id=account.user_id, image_url=avatar_url)
      profile.save()
      profile.get_remote_image()
    

@receiver(pre_social_login)
def test1(sender, **kwargs):
  print '-------------  pre_social_login'
  account = kwargs['sociallogin'].account
  print account.user_id
  print account.uid
  print account.provider

  # if account.provider == 'facebook' and not Profile.objects.filter(user_id=account.user_id).exists():
    # avatar_url =   GRAPH_API_URL + '/%s/picture?type=square&height=300&width=300&return_ssl_resources=1' % account.uid  # noqa
    # Profile.objects.create(user_id=account.user_id, picture=avatar_url)
    




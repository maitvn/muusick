from django.contrib import admin
from django.contrib.admin import AdminSite
from models import *
from django.utils.html import format_html


AdminSite.site_header = 'Muusick admin'
AdminSite.site_title = 'Muusick admin'
admin.ModelAdmin.list_per_page = 50


class MyAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all': ('css/my_styles.css',)
        }

    def logo_tag(self, obj):
        return u'<img src="/media/%s" width="100" />' % obj.logo

    logo_tag.short_description = 'Image'
    logo_tag.allow_tags = True


@admin.register(Notification)
class NotificationAdmin(MyAdmin):
    list_display = ('id', 'user', 'message', 'internal_link', )


@admin.register(Genre)
class GenreAdmin(MyAdmin):
    def img_tag(self, obj):
        return u'<img src="/media/%s" width="100" />' % obj.image if obj.image else '-'

    img_tag.short_description = 'Image'
    img_tag.allow_tags = True
    list_editable = ('display_order', 'country', 'note',)
    list_display = ('id', 'img_tag', 'name', 'display_order', 'country', 'status', 'note', )
    search_fields = ['name']


@admin.register(Mood)
class MoodAdmin(MyAdmin):
    def img_tag(self, obj):
        return u'<img src="/media/%s" width="100" />' % obj.image if obj.image else '-'

    img_tag.short_description = 'Image'
    img_tag.allow_tags = True
    list_editable = ('display_order',)
    list_display = ('id', 'img_tag', 'name', 'display_order',)


@admin.register(Country)
class CountryAdmin(MyAdmin):
    list_display = ('id', 'name', )


@admin.register(Artist)
class ArtistAdmin(MyAdmin):
    def img_tag(self, obj):
        return u'<img src="/media/%s" width="100" />' % obj.image if obj.image else '-'

    img_tag.short_description = 'Image'
    img_tag.allow_tags = True
    list_display = ('id', 'img_tag', 'name', 'image', 'is_feature', 'gender', 'band', 'country', 'genre', 'tag', 'youtube_channel', 'score', 'note',)
    list_editable = ('is_feature', 'youtube_channel', 'image', 'gender', 'band', 'country', 'genre', 'tag', 'score', 'note',)
    search_fields = ['name']


@admin.register(Tag)
class TagAdmin(MyAdmin):
    def img_tag(self, obj):
        return u'<img src="/media/%s" width="100" />' % obj.image if obj.image else '-'

    img_tag.short_description = 'Image'
    img_tag.allow_tags = True
    list_display = ('id',  'img_tag', 'name', 'is_instrument', 'is_hot', 'display_order', 'status', 'note',)
    list_editable = ('display_order', 'is_instrument', 'is_hot', 'status', 'note',)
    search_fields = ['name']


@admin.register(Video)
class VideoAdmin(MyAdmin):
    def img_tag(self, obj):
        return u'<img src="http://img.youtube.com/vi/%s/0.jpg" width="100" />' % obj.yt_id

    img_tag.short_description = 'Image'
    img_tag.allow_tags = True

    def get_tags(self, obj):
        return ",".join([t.name for t in obj.tags.all()])

    list_editable = ('is_feature', 'country', 'display_order', )
    list_display = ('id', 'img_tag', 'title', 'is_feature', 'get_tags', 'display_order', 'country', 'note', 'status')
    list_filter = ('genres', 'note',)
    search_fields = ['title', 'yt_id']


@admin.register(Playlist)
class PlaylistAdmin(MyAdmin):
    def img_tag(self, obj):
        return u'<img src="/media/%s" width="100" />' % obj.image if obj.image else '-'

    img_tag.short_description = 'Image'
    img_tag.allow_tags = True

    list_editable = ('is_top_chart', 'is_feature', 'is_official', 'display_order', 'country', )

    list_display = ('id', 'img_tag', 'name', 'display_order', 'is_top_chart',  'is_feature', 'is_official', 'country', )
    search_fields = ['name']


@admin.register(Activity)
class ActivityAdmin(MyAdmin):
    list_display = ('id', 'action', 'message', 'playlist_id', 'video_id', 'target_user', 'trigger_user', )
    raw_id_fields = ('playlist', 'video', )


@admin.register(ImportPlaylist)
class ImportPlaylistAdmin(MyAdmin):
    def import_button(self, obj):
        if obj.status == 0:
            return format_html(u'<button id="{}" class="btn button import-btn" type="button">Import</button>', obj.id)
        if obj.status == 1:
            return format_html(u'<button id="{}" class="btn button import-btn" type="button">Re-Run</button>', obj.id)

    import_button.short_description = 'Import'
    list_display = ('id', 'type', 'playlist', 'yt_playlist_url', 'is_override', 'import_at',  'status', 'import_button' )
    raw_id_fields = ('playlist', )

    class Media:
        js = ("js/import_playlist_admin.js", )
        css = {
            "all": ("css/import_playlist_admin.css", )
        }

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.http import urlquote
from django.contrib.auth.models import User
from django.core.files import File
import os
import urllib
from django.core.urlresolvers import reverse
import uuid
from django.conf import settings


class PlaylistQuerySet(models.QuerySet):
    def public(self):
        return self.filter(is_public=True)


class PlaylistManager(models.Manager):
    def get_queryset(self):
        return PlaylistQuerySet(self.model, using=self._db)

    def public(self):
        return self.get_queryset().public()


class Profile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    picture = models.ImageField(upload_to='users/', blank=True, null=True)
    bio = models.TextField(blank=True)
    fullname = models.CharField(max_length=100, blank=True)
    is_curator = models.BooleanField(default=False)
    url = models.CharField(max_length=512, blank=True)
    image_url = models.URLField(blank=True)

    @property
    def secure_email(self):
        email = self.user.email
        if email:
            email_parts = email.split('@')
            pivot = int(len(email_parts[0]) / 2)
            return '%s%s@%s' % (
                email_parts[0][:pivot],
                '*' * (len(email_parts[0]) - pivot),
                email_parts[1],
            )
        return ''

    def get_remote_image(self):
        if self.image_url and not self.picture:
            result = urllib.urlretrieve(self.image_url)
            self.picture.save(
                '%s%d.jpg' % ('user_', self.user_id),
                File(open(result[0]))
            )
            self.save()


class Category(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=128, blank=True)
    parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True, related_name='+', )
    image = models.ImageField(upload_to='categories/', blank=True, null=True)
    display_order = models.SmallIntegerField(null=True, blank=True)

    @property
    def songs_count(self):
        return Video.objects.filter(category=self).count()

    class Meta:
        ordering = ('display_order',)

    def __unicode__(self):
        return u'%s' % self.name


class Genre(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=128, blank=True)
    image = models.ImageField(upload_to='genres/', blank=True, null=True)
    display_order = models.SmallIntegerField(null=True, blank=True)

    class Meta:
        ordering = ('display_order',)

    def __unicode__(self):
        return u'%s' % self.name


class Mood(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=128, blank=True)
    image = models.ImageField(upload_to='moods/', blank=True, null=True)
    display_order = models.SmallIntegerField(null=True, blank=True)

    class Meta:
        ordering = ('display_order',)

    def __unicode__(self):
        return u'%s' % self.name


class Tag(models.Model):
    name = models.CharField(max_length=128, db_index=True)
    description = models.CharField(max_length=128, blank=True)
    image = models.ImageField(upload_to='tags/', blank=True, null=True)
    is_instrument = models.BooleanField(default=False, db_index=True)
    display_order = models.SmallIntegerField(null=True, blank=True, db_index=True)
    is_hot = models.BooleanField(default=False, db_index=True)

    class Meta:
        ordering = ['-display_order']

    def __unicode__(self):
        return u'%s' % self.name


class TagCombination(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=128, blank=True)
    image = models.ImageField(upload_to='tags/', blank=True, null=True)
    tags = models.ManyToManyField(Tag, blank=True)
    display_order = models.SmallIntegerField(null=True, blank=True)

    def __unicode__(self):
        return u'%s' % self.name


class Artist(models.Model):
    name = models.CharField(max_length=512, db_index=True)
    description = models.CharField(max_length=512, null=True, blank=True)
    url = models.CharField(max_length=512, null=True, blank=True)
    image = models.ImageField(upload_to='artists/', blank=True, null=True)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return u'%s' % self.name


class Video(models.Model):
    title = models.CharField(max_length=512, db_index=True)
    description = models.TextField(blank=True)
    url = models.CharField(max_length=512, blank=True)
    yt_id = models.CharField(max_length=32, db_index=True)
    category = models.ForeignKey(Category, null=True, blank=True, related_name='videos')
    tags = models.ManyToManyField(Tag, blank=True, related_name='tags')
    genres = models.ManyToManyField(Genre, blank=True, related_name='genres')
    mood = models.ForeignKey(Mood, null=True, blank=True)
    artists = models.ManyToManyField(Artist, blank=True, related_name='artists')
    curator = models.ForeignKey(User, null=True, blank=True)
    is_feature = models.BooleanField(default=False, db_index=True)
    favorites = models.ManyToManyField(User, through='FavoriteVideo', related_name="favorite_videos")
    history = models.ManyToManyField(User, through='HistoryVideo', related_name="history_videos")

    display_order = models.IntegerField(null=True, blank=True, db_index=True)
    score = models.IntegerField(null=True, blank=True, default=0, db_index=True)
    view_count = models.IntegerField(null=True, blank=True, default=0, db_index=True)
    favorite_count = models.IntegerField(null=True, blank=True, default=0, db_index=True)

    status = models.SmallIntegerField(null=True, blank=True, default=0, db_index=True)

    updated_at = models.DateField(auto_now=True, null=True)
    created_at = models.DateField(auto_now_add=True, null=True)

    def __unicode__(self):
        return u'%s' % self.title


class Playlist(models.Model):
    slug = models.UUIDField(default=uuid.uuid4, editable=False, db_index=True)
    name = models.CharField(max_length=512, db_index=True)
    description = models.TextField()
    image = models.ImageField(upload_to='playlist/', blank=True, null=True)
    category = models.ForeignKey(Category, null=True, blank=True, related_name='playlist_categories')
    tags = models.ManyToManyField(Tag, blank=True, related_name="playlist_tags")
    genres = models.ManyToManyField(Genre, blank=True, related_name='playlist_genres')
    mood = models.ForeignKey(Mood, null=True, blank=True, related_name="playlist_moods")
    creator = models.ForeignKey(User, null=True, blank=True)
    artist_tags = models.CharField(max_length=512, null=True, blank=True)
    videos = models.ManyToManyField(Video, through='PlaylistVideo')
    is_feature = models.BooleanField(default=False, db_index=True)

    is_official = models.BooleanField(default=False, db_index=True)

    is_public = models.BooleanField(default=False, db_index=True)

    following = models.ManyToManyField(User, through='FollowPlaylist', related_name="following_playlists")
    history = models.ManyToManyField(User, through='HistoryPlaylist', related_name="history_playlists")

    contributors = models.ManyToManyField(User, through='Collaborator', related_name="contributors")

    display_order = models.IntegerField(null=True, blank=True, db_index=True)
    score = models.IntegerField(null=True, blank=True, default=0, db_index=True)
    view_count = models.IntegerField(null=True, blank=True, default=0, db_index=True)
    follow_count = models.IntegerField(null=True, blank=True, default=0, db_index=True)

    status = models.SmallIntegerField(null=True, blank=True, default=0, db_index=True)

    updated_at = models.DateField(auto_now=True, null=True)
    created_at = models.DateField(auto_now_add=True, null=True)

    objects = PlaylistManager()

    @property
    def share_url(self):
        return '%s%s' % (settings.HOST_URL, reverse('share_url', kwargs={'slug': self.slug}))

    def __unicode__(self):
        return u'%s' % self.name


class Collaborator(models.Model):
    playlist = models.ForeignKey(Playlist)
    user = models.ForeignKey(User, null=True)
    status = models.SmallIntegerField(null=True, default=0)
    joined_at = models.DateField(auto_now_add=True, null=True)
    invite_key = models.UUIDField(default=uuid.uuid4, editable=True)
    email = models.EmailField(null=True)

    @property
    def invite_url(self):
        return '%s%s' % (settings.HOST_URL, reverse('invite_url', kwargs={'key': self.invite_key}))

    class Meta:
        ordering = ('-joined_at',)


class PlaylistVideo(models.Model):
    video = models.ForeignKey(Video)
    playlist = models.ForeignKey(Playlist)
    added_by = models.ForeignKey(User, null=True)
    added_at = models.DateField(auto_now_add=True, null=True)

    class Meta:
        ordering = ('-added_at',)


class FollowUser(models.Model):
    from_user = models.ForeignKey(User, null=True, related_name="from_user")
    to_user = models.ForeignKey(User, null=True, related_name="to_user")
    follow_at = models.DateField(auto_now_add=True, null=True)

    class Meta:
        ordering = ('-follow_at',)


class FollowPlaylist(models.Model):
    user = models.ForeignKey(User, null=True)
    playlist = models.ForeignKey(Playlist, null=True)
    follow_at = models.DateField(auto_now_add=True, null=True)

    class Meta:
        ordering = ('-follow_at',)


class FollowCategory(models.Model):
    following = models.ForeignKey(User)
    followed = models.ForeignKey(Category)
    follow_at = models.DateField(auto_now_add=True, null=True)

    class Meta:
        ordering = ('-follow_at',)


class FavoriteVideo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    video = models.ForeignKey(Video, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True, null=True)

    class Meta:
        ordering = ('-created_at',)


class HistoryVideo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    video = models.ForeignKey(Video, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True, null=True)

    class Meta:
        ordering = ('-created_at',)


class HistoryPlaylist(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True, null=True)

    class Meta:
        ordering = ('-created_at',)

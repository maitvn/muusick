from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from models import *
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static
import re
import json
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.views.decorators.cache import cache_page


def index(request):
    devices = mobile(request)['device']
    if 'iphone' in devices or 'android' in devices:
        # return render(request, 'myapp/mobile_detect.html', {})
        return render(request, 'myapp/index.html', {'mobile': True})
    else:
        return render(request, 'myapp/index.html', {'mobile': False})


def landing(request):
    language_str = request.META.get('HTTP_ACCEPT_LANGUAGE', '').lower()

    selected_lang = request.GET['lang'] if 'lang' in request.GET else None

    if selected_lang:
        if selected_lang == 'en':
            return render(request, 'myapp/landing_en.html', {})
        else:
            return render(request, 'myapp/landing_jp.html', {})
    else:
        if 'en-us' in language_str:
            return render(request, 'myapp/landing_en.html', {})
        else:
            return render(request, 'myapp/landing_jp.html', {})


@cache_page(60 * 60)
def playlist(request, slug):
    try:
        playlist = Playlist.objects.get(slug=slug)
        videos = json.dumps([{'yt_id': v.yt_id, 'title': v.title} for v in playlist.videos.all()])
        image = '/media/%s' % playlist.image if playlist.image else static('myapp/img/default_playlist.png')
    except ObjectDoesNotExist:
        return render(request, 'myapp/playlist404.html', {})
    except ValueError:
        return render(request, 'myapp/playlist404.html', {
            'related_playlists': Playlist.objects.order_by('-view_count')[:7]
            })
    return render(request, 'myapp/playlist.html', {
        'playlist': playlist,
        'videos_json': videos,
        'related_playlists': Playlist.objects.order_by('-view_count')[:6],
        'HOST_URL': settings.HOST_URL,
        'image': '%s%s' % (settings.HOST_URL, image)
    })


def discover(request):
    return render(request, 'myapp/discover.html', {'playlists': Playlist.objects.all()})


@login_required
def confirm_invite_playlist(request, key):

    invite = Collaborator.objects.filter(invite_key=key).first()

    if invite:
        return render(request, 'myapp/confirm_invite_playlist.html', {
            'invitation': invite
        })

    return redirect('/')


def mobile(request):
    device = {}

    ua = request.META.get('HTTP_USER_AGENT', '').lower()

    if ua.find("iphone") > 0:
        device['iphone'] = "iphone" + re.search("iphone os (\d)", ua).groups(0)[0]

    if ua.find("ipad") > 0:
        device['ipad'] = "ipad"

    if ua.find("android") > 0:
        device['android'] = "android" + re.search("android (\d\.\d)", ua).groups(0)[0].translate(None, '.')

    if ua.find("blackberry") > 0:
        device['blackberry'] = "blackberry"

    if ua.find("windows phone os 7") > 0:
        device['winphone7'] = "winphone7"

    if ua.find("iemobile") > 0:
        device['winmo'] = "winmo"

    if not device:  # either desktop, or something we don't care about.
        device['baseline'] = "baseline"

    # spits out device names for CSS targeting, to be applied to <html> or <body>.
    device['classes'] = " ".join(v for (k, v) in device.items())

    return {'device': device}
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .serializers import *
from myapp.models import *
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static
from HTMLParser import HTMLParser
from time import sleep


@csrf_exempt
def get_video(request):
    yt_id = request.GET['yt_id'] if 'yt_id' in request.GET else ''
    if Video.objects.filter(yt_id=yt_id).exists():
        return JsonResponse({
            'video': True
        })
    else:
        return JsonResponse({
            'video': None
        })


@csrf_exempt
def lists(request):
    if request.user.is_anonymous():
        return JsonResponse({
            'error': 'Please login'
        }, status=310)

    playlists = PlaylistSerializer(Playlist.objects.filter(creator=request.user).order_by('-updated_at')[:50],
                                   many=True,
                                   context={'request': request})
    _collaboration_playlists = [c.playlist for c in Collaborator.objects.filter(user=request.user, status=1)]
    collaboration_playlists = PlaylistSerializer(_collaboration_playlists,
                                                 many=True,
                                                 context={'request': request})
    return JsonResponse({
        'playlists': playlists.data + collaboration_playlists.data,
        'default_image': settings.HOST_URL + static('myapp/img/default_playlist.png'),
    })


@csrf_exempt
def browse_lists(request):
    if request.user.is_anonymous():
        trending_playlists = PlaylistSerializer(
            Playlist.objects.public().filter(image__isnull=False).order_by('-view_count')[:12],
            many=True,
            context={'request': request}
        )

        return JsonResponse({
            'host_url': settings.HOST_URL,
            'username': '',
            'default_image': settings.HOST_URL + static('myapp/img/default_playlist.png'),
            'my_playlists': [],
            'recent_playlists': [],
            'trending_playlists': trending_playlists.data,
        })
    else:
        my_playlists = PlaylistSerializer(
            Playlist.objects.filter(creator=request.user).order_by('-updated_at')[:6],
            many=True,
            context={'request': request})

        _collaboration_playlists = [c.playlist for c in Collaborator.objects.filter(user=request.user, status=1)]
        collaboration_playlists = PlaylistSerializer(_collaboration_playlists,
                                                     many=True,
                                                     context={'request': request})

        playlists = [f.playlist for f in HistoryPlaylist.objects.filter(playlist__is_public=True, user=request.user)[:6]]
        history_playlists = PlaylistSerializer(playlists, many=True, context={'request': request})

        trending_playlists = PlaylistSerializer(
            Playlist.objects.public().filter(image__isnull=False).order_by('-view_count')[:6],
            many=True,
            context={'request': request}
        )

        return JsonResponse({
            'host_url': settings.HOST_URL,
            'username': request.user.username,
            'default_image': settings.HOST_URL + static('myapp/img/default_playlist.png'),
            'my_playlists': my_playlists.data + collaboration_playlists.data,
            'recent_playlists': history_playlists.data,
            'trending_playlists': trending_playlists.data,
        })


@login_required
@csrf_exempt
def create_list(request):
    if request.method == 'POST':
        name = request.POST['name']
        description = request.POST['description']
        playlist = Playlist.objects.create(
            name=name,
            description=description,
            is_public=False,
            creator=request.user
        )

        return JsonResponse({
            'playlist': PlaylistSerializer(playlist, many=False, context={'request': request}).data
        })


@login_required
@csrf_exempt
def add_2_list(request):
    if request.method == 'POST':
        h = HTMLParser()
        title = h.unescape(request.POST['title'])
        yt_id = request.POST['yt_id']
        playlist_id = request.POST['playlist_id']
        flag = bool(int(request.POST['flag']))
        video = Video.objects.filter(yt_id=yt_id).first()
        if not video:
            video = Video()
            video.title = title
            video.yt_id = yt_id
            video.curator = request.user
            video.save()

        playlist = Playlist.objects.get(id=playlist_id)
        instance = PlaylistVideo.objects.filter(playlist=playlist, video=video).first()
        if flag and not instance:
            PlaylistVideo.objects.create(playlist=playlist, video=video)
        if not flag and instance:
            instance.delete()

        return JsonResponse({})


@csrf_exempt
def init(request):
    """
    Init popup data
    :param request:
    :return:
    """
    if request.user.is_anonymous():
        return JsonResponse({
            'error': 'Please login'
        }, status=310)

    if request.method == 'GET':

        video = Video.objects.filter(yt_id=request.GET['yt_id']).first()
        if video:
            video_serializer = VideoSerializer(
                video,
                many=False
            )
            video_info = video_serializer.data
        else:
            video_info = {
                'tags': [],
                'artists': []
            }

        genres = GenreSerializer(
            Genre.objects.all(),
            many=True,
            context={'request': request}
        )

        instruments = TagSerializer(
            Tag.objects.filter(is_instrument=True),
            many=True,
            context={'request': request, 'yt_id': request.GET['yt_id']}
        )

        hot_tags = TagSerializer(
            Tag.objects.filter(is_hot=True),
            many=True,
            context={'request': request, 'yt_id': request.GET['yt_id']}
        )

        user_playlists = PlaylistSerializer(
            Playlist.objects.filter(creator=request.user).order_by('-updated_at'),
            many=True,
            context={'request': request}
        )
        _collaboration_playlists = [c.playlist for c in Collaborator.objects.filter(user=request.user, status=1)]
        collaboration_playlists = PlaylistSerializer(_collaboration_playlists,
                                                     many=True,
                                                     context={'request': request})
        print '-----', request.user.is_staff
        return JsonResponse({
            'genres': genres.data,
            'instruments': instruments.data,
            'hot_tags': hot_tags.data,
            'user_playlists': user_playlists.data + collaboration_playlists.data,
            'video': video_info,
            'is_staff': request.user.is_staff
        })
    return JsonResponse({
        'error': 'GET method is required'
    })


@login_required
@csrf_exempt
def update_artists(request):
    """
    Update artist (add or delete)
    :param request:
    :return:
    """

    h = HTMLParser()
    title = h.unescape(request.POST['title'])

    video, created = Video.objects.get_or_create(
        yt_id=request.POST['yt_id'],
        defaults={
            'title': title,
            'curator': request.user
        }
    )

    artist, created = Artist.objects.get_or_create(
        name=request.POST['artist_name']
    )

    print artist

    if bool(int(request.POST['flag'])):
        video.artists.add(artist)
        video.save()
        return JsonResponse(ArtistSerializer(artist, many=False).data)
    else:
        video.artists.remove(artist)
        video.save()

    return JsonResponse({})


@login_required
@csrf_exempt
def update_tags(request):
    """
    Update tags (add or delete)
    :param request:
    :return:
    """
    h = HTMLParser()
    title = h.unescape(request.POST['title'])

    video, created = Video.objects.get_or_create(
        yt_id=request.POST['yt_id'],
        defaults={
            'title': title,
            'curator': request.user
        }
    )

    if 'tag_id' in request.POST:
        tag = Tag.objects.get(
            id=request.POST['tag_id']
        )
    elif 'tag_name' in request.POST:
        tag, tag_created = Tag.objects.get_or_create(
            name=request.POST['tag_name']
        )

    if bool(int(request.POST['flag'])):
        video.tags.add(tag)
        video.save()
        return JsonResponse(TagSerializer(tag, many=False).data)
    else:
        video.tags.remove(tag)
        video.save()

    return JsonResponse({})


@login_required
@csrf_exempt
def update_genres(request):
    """
    Update genres (add or delete)
    :param request:
    :return:
    """

    h = HTMLParser()
    title = h.unescape(request.POST['title'])

    video, created = Video.objects.get_or_create(
        yt_id=request.POST['yt_id'],
        defaults={
            'title': title,
            'curator': request.user
        }
    )

    genre = Genre.objects.get(
        id=request.POST['genre_id']
    )

    if bool(int(request.POST['flag'])):
        video.genres.add(genre)
        video.save()
    else:
        video.genres.remove(genre)
        video.save()

    return JsonResponse({})


@csrf_exempt
def get_tag_suggestions(request):
    """
    Get tag suggestions
    :param request:
    :return:
    """
    tags = list(set([t.name for t in Tag.objects.filter(name__icontains=request.GET['text'])[:4]]))
    return JsonResponse({'tags': tags})


@csrf_exempt
def get_artist_suggestions(request):
    """
    Get artists suggestions
    :param request:
    :return:
    """
    artists = list(set([t.name for t in Artist.objects.filter(name__icontains=request.GET['text'])[:4]]))
    return JsonResponse({'artists': artists})

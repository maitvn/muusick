# -*- coding: utf-8 -*-
from rest_framework import serializers
from myapp.models import *


class RequestSerializer(serializers.Serializer):
    title = serializers.CharField()
    yt_id = serializers.CharField()



### Model serialzers
class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ('id', 'name', 'is_added', 'image')

    is_added = serializers.SerializerMethodField()

    def get_is_added(self, obj):
        if 'yt_id' in self.context['request'].GET:
            yt_id = self.context['request'].GET['yt_id']
            return Video.objects.filter(yt_id=yt_id, genres__in=[obj]).exists()
        return False


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name', 'is_added', 'image', )

    is_added = serializers.SerializerMethodField()

    def get_is_added(self, obj):
        if 'yt_id' in self.context:
            yt_id = self.context['yt_id']
            return Video.objects.filter(yt_id=yt_id, tags__in=[obj]).exists()
        return False


class PlaylistSerializer(serializers.ModelSerializer):
    is_added = serializers.SerializerMethodField()

    def get_is_added(self, obj):
        if 'yt_id' in self.context['request'].GET:
            yt_id = self.context['request'].GET['yt_id']
            video = Video.objects.filter(yt_id=yt_id).first()
            if video:
                return video in obj.videos.all()
        return False

    class Meta:
        model = Playlist
        fields = ('id', 'name', 'image', 'is_added', 'share_url', 'slug', )


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = ('id', 'name',)


class VideoSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    artists = ArtistSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = ('id', 'title', 'yt_id', 'tags', 'artists')

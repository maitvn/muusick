# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
import urllib2,urllib
from bs4 import BeautifulSoup
from django.core.files import File
import ntpath
from myapp.models import Artist


class Command(BaseCommand):
    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        """
        Send notification batch

        """

        for artist in Artist.objects.exclude(youtube_channel=u'').filter(youtube_channel__isnull=False, status=0):
            print '>>> Extracting: ', artist.youtube_channel
            try:
                page = urllib2.urlopen('%s/about' % artist.youtube_channel).read()
                soup = BeautifulSoup(page, 'html.parser')

                # Image
                img = soup.find_all("img", class_="channel-header-profile-image")
                if img:
                    # TODO set artist object
                    remote_image = urllib.urlretrieve(img[0]['src'])
                    artist.image.save(
                        ntpath.basename(img[0]['src']),
                        File(open(remote_image[0]))
                    )

                # Desc
                desc = soup.find_all('div', class_='about-description branded-page-box-padding')

                if desc:
                    artist.description = desc[0].get_text()

                artist.status = 1
                artist.save()
            except:
                print '----ERROR!'




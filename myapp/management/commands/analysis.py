from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.conf import settings
from myapp.models import *
from HTMLParser import HTMLParser


class Command(BaseCommand):
    help = 'calculate view count, songs count for playlist and video'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):

        for playlist in Playlist.objects.all():
            print '...%s' % playlist.name
            playlist.follow_count = FollowPlaylist.objects.filter(playlist=playlist).count()
            playlist.song_count = playlist.videos.count()
            playlist.video_count = playlist.videos.count()

            if playlist.is_public and (playlist.song_count < 10 or not playlist.image):
                playlist.is_public = False

            playlist.save()

        for video in Video.objects.all():
            print '...%s' % video.title
            video.favorite_count = FavoriteVideo.objects.filter(video=video).count()
            video.save()


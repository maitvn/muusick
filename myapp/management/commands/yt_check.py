from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.conf import settings
from myapp.models import *
import uuid
from django.test import Client
from django.core.urlresolvers import reverse
import urllib2,urllib


class Command(BaseCommand):
    help = 'Preheat cache data'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        client = Client()

        # print '> Preheat: Main'
        # response = client.get(reverse('api_initial'))
        # print '...........', response.status_code

        for video in Video.objects.all():
            self.stdout.write(self.style.SUCCESS('...Runing: %s' % video.yt_id))
            try:
                page = urllib2.urlopen('https://img.youtube.com/vi/%s/default.jpg' % video.yt_id)
            except urllib2.HTTPError as e:
                self.stdout.write(self.style.NOTICE('...Error: %s' % video.yt_id))
                video.status = 99
                video.save()


# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from myapp.models import *


class Command(BaseCommand):
    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        """
        Country Genres

        """

        for genre in Genre.objects.filter(country__isnull=False).all():
            print '[Run] >> :', genre.country

            Video.objects.filter(genres__in=[genre]).update(
                country=genre.country
            )

            Playlist.objects.filter(genres__in=[genre]).update(
                country=genre.country
            )

            for video in Video.objects.filter(country=genre.country).all():
                video.genres.add(genre)
                video.save()

            for playlist in Playlist.objects.filter(country=genre.country).all():
                playlist.genres.add(genre)
                playlist.save()

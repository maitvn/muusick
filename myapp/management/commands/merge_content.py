from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.conf import settings
from myapp.models import *
import uuid
from django.test import Client
from django.core.urlresolvers import reverse


class Command(BaseCommand):
    help = 'Preheat cache data'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):

        tag_2_genre = (
            (95, 16, ),  # vocaloid
            (108, 27, ),  # techno
            (43, 1, ),  # rock
            (65, 12, ),  # Reggae
            (66, 17, ),  # R&B
            (180, 32, ),  # Progressive
            (138, 32, ),  # Progressive
            (68, 4, ),  # Pop
            (41, 4, ),  # Pop
            (42, 3, ),  # Jazz
            (182, 9, ),  # heavy metal
            (44, 9, ),  # Heavy metal
            (64, 31, ),  # funk
            (60, 5, ),  # edm/dance
            (55, 5, ),  # edm/dance
            (49, 10, ),  # country
            (124, 2, ),  # classic
            (167, 2, ),  # classic
            (63, 30, ),  # blues
            (148, 7, ),  # anime
            (157, None, ),  # NEW
            (153, None, ),  # New
        )

        for item in tag_2_genre:

            tag = Tag.objects.filter(id=item[0]).first()
            if not tag:
                continue

            if item[1]:
                genre = Genre.objects.filter(id=item[1]).first()
            else:
                print '---Create new tag: %s' % tag.name
                genre = Genre.objects.create(
                    name=tag.name
                )
            if not genre:
                continue

            print item[0], '----', item[1]

            for video in Video.objects.filter(tags=tag):
                video.tags.remove(tag)
                video.genres.add(genre)
                video.save()

            tag.delete()


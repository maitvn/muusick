# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
import urllib2, urllib
from bs4 import BeautifulSoup
from myapp.models import *


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('params', nargs='+', type=str)

        parser.add_argument(
            '--update',
            action='store_true',
            dest='update',
            default=False,
            help='Update to playlist',
        )

        parser.add_argument(
            '--new',
            action='store_true',
            dest='new',
            default=False,
            help='Update to playlist',
        )

        parser.add_argument(
            '--artist',
            action='store_true',
            dest='artist',
            default=False,
            help='Update to playlist',
        )

        parser.add_argument(
            '--merge',
            action='store_true',
            dest='merge',
            default=False,
            help='Update to playlist',
        )

    def handle(self, *args, **options):
        # validate
        if len(options['params']) < 2:
            self.stdout.write(self.style.NOTICE('Too few arguments (URL, id)'))
            return

        if options['new'] or options['update']:
            playlist = None
            if options['new']:
                name = options['params'][1]
                if not isinstance(name, basestring) or len(name) < 2:
                    self.stdout.write(self.style.NOTICE('Not valid playlist name '))
                    return

                if raw_input('Add youtube mixes to new playlist [%s] (Y/n): ' % name).lower() == 'y':
                    playlist = Playlist.objects.create(
                        name=name,
                        creator_id=1,
                        description='Muusick official playlist'
                    )
                else:
                    return

            else:
                try:
                    playlist = Playlist.objects.filter(id=options['params'][1]).first()
                except:
                    self.stdout.write(self.style.NOTICE('Not valid playlist ID....'))
                    return
                if not playlist:
                    self.stdout.write(self.style.NOTICE('Not valid playlist ID....'))
                    return

                if raw_input('Add youtube mixes to playlist [%s] (Y/n): ' % playlist.name).lower() != 'y':
                    return

            try:
                if not options['merge']:
                    PlaylistVideo.objects.filter(playlist_id=playlist.id).delete()
                page = urllib2.urlopen(options['params'][0]).read()
                soup = BeautifulSoup(page, 'html.parser')
                # Image
                videos = soup.find_all("li", class_="yt-uix-scroller-scroll-unit")

                for video in videos:
                    video_object = Video.objects.filter(yt_id=video['data-video-id']).first()
                    if not video_object:
                        video_object = Video.objects.create(
                            yt_id=video['data-video-id'],
                            title=video['data-video-title']
                        )

                    PlaylistVideo.objects.update_or_create(
                        playlist_id=playlist.id,
                        video_id=video_object.id
                    )
            except:
                print '---- EXTRACT ERROR!'

        elif options['artist']:
            artist = Artist.objects.filter(id=options['params'][1]).first()
            if not artist:
                self.stdout.write(self.style.NOTICE('Not valid artist ID....'))
                return
            self.stdout.write(self.style.NOTICE('Add to %s ' % artist.name ))

            yn = raw_input('Add youtube mixes to %s (Y/n): ' % artist.name)

            if yn.lower() != 'y':
                return

            try:
                page = urllib2.urlopen(options['params'][0]).read()
                soup = BeautifulSoup(page, 'html.parser')
                # Image
                videos = soup.find_all("li", class_="yt-uix-scroller-scroll-unit")

                for video in videos:
                    print '%s  - %s -- %s ' % (
                    video['data-video-id'], video['data-video-username'], video['data-video-title'])
                    video_object = Video.objects.filter(yt_id=video['data-video-id']).first()
                    if not video_object:
                        video_object = Video.objects.create(
                            yt_id=video['data-video-id'],
                            title=video['data-video-title']
                        )
                    video_object.artists.add(artist)
                    video_object.save()

            except:
                print '---- EXTRACT ERROR!'

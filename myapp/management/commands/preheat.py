from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.conf import settings
from myapp.models import *
import uuid
from django.test import Client
from django.core.urlresolvers import reverse


class Command(BaseCommand):
    help = 'Preheat cache data'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        client = Client()

        # print '> Preheat: Main'
        # response = client.get(reverse('api_initial'))
        # print '...........', response.status_code

        print '> Preheat: All lists API'
        response = client.get(reverse('api_get_playlist_list'))
        print '...........', response.status_code

        for genre in Genre.objects.all():
            print '> Preheat: Genres: %s' % genre.name
            response = client.get(reverse('browse', kwargs={'type': 'genre', 'id': genre.id, 'order': 'random'}))
            print '......random.....', response.status_code

            response = client.get(reverse('browse', kwargs={'type': 'genre', 'id': genre.id, 'order': 'time'}))
            print '......time.....', response.status_code

            response = client.get(reverse('browse', kwargs={'type': 'genre', 'id': genre.id, 'order': 'trending'}))
            print '......trending.....', response.status_code

        for tag in Tag.objects.all():
            print '> Preheat: Tags'
            response = client.get(reverse('browse', kwargs={'type': 'tag', 'id': tag.id, 'order': 'random'}))
            print '......random.....', response.status_code

            response = client.get(reverse('browse', kwargs={'type': 'tag', 'id': tag.id, 'order': 'time'}))
            print '......time.....', response.status_code

            response = client.get(reverse('browse', kwargs={'type': 'tag', 'id': tag.id, 'order': 'trending'}))
            print '......trending.....', response.status_code

        for playlist in Playlist.objects.filter(is_public=True):
            print '> Preheat Playlist: %s' % playlist.name

            response = client.get(reverse('api_get_playlist_data'), {'slug': playlist.slug})
            print '...........', response.status_code

            response = client.get(reverse('share_url', kwargs={'slug': playlist.slug}),)
            print '...........', response.status_code

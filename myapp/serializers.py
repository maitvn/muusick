# -*- coding: utf-8 -*-
from rest_framework import serializers
from django.utils.http import urlencode, urlquote
from models import *
from django.contrib.auth.models import User
import json


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'songs_count')


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name', 'image')


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ('id', 'link', 'status', 'internal_link', 'image_url', 'message', 'added_at')


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ('id', 'name', 'is_added', 'image')

    is_added = serializers.SerializerMethodField()

    def get_is_added(self, obj):
        if 'yt_id' in self.context['request'].GET:
            yt_id = self.context['request'].GET['yt_id']
            return Video.objects.filter(yt_id=yt_id, genres__in=[obj]).exists()
        return False


class TagCombinationSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagCombination
        fields = ('id', 'name', 'tags')


class UserSerializer(serializers.ModelSerializer):
    fullname = serializers.CharField(source='profile.fullname')
    picture = serializers.ImageField(source='profile.picture')

    class Meta:
        model = User
        fields = ('username', 'fullname', 'picture')


class UserDetailSerializer(serializers.ModelSerializer):
    picture = serializers.ImageField(source='profile.picture')
    bio = serializers.CharField(source='profile.bio')
    fullname = serializers.CharField(source='profile.fullname')
    secure_email = serializers.CharField(source='profile.secure_email')
    is_following = serializers.SerializerMethodField()

    def get_is_following(self, obj):
        user = self.context['request'].user
        if not user.is_anonymous():
            if FollowUser.objects.filter(from_user=user, to_user=obj).exists():
                return True

        return False

    class Meta:
        model = User
        fields = ('username', 'secure_email', 'is_following', 'fullname', 'picture', 'bio')


class UserTagSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=512)
    count = serializers.IntegerField()


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = ('id', 'name', 'image')


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('id', 'title', 'yt_id', )


class VideoDetailSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)
    genres = GenreSerializer(many=True, read_only=True)
    artists = ArtistSerializer(many=True, read_only=True)

    class Meta:
        model = Video
        fields = ('id', 'title', 'yt_id', 'artists', 'tags', 'genres', )


class ContributorSerializer(serializers.HyperlinkedModelSerializer):
    username = serializers.ReadOnlyField(source='user.username')
    fullname = serializers.ReadOnlyField(source='user.profile.fullname')
    picture = serializers.ReadOnlyField(source='user.profile.picture.url')

    class Meta:
        model = Collaborator
        fields = ('username', 'email', 'picture', 'fullname', 'status', 'joined_at')


class PlaylistSerializer(serializers.ModelSerializer):
    creator = UserSerializer(many=False, read_only=True)

    class Meta:
        model = Playlist
        fields = ('id', 'name', 'slug', 'share_url',
                  'is_public', 'is_collaborative', 'description', 'video_count', 'image', 'creator', )


class PlaylistDetailSerializer(serializers.ModelSerializer):
    creator = UserSerializer()

    class Meta:
        model = Playlist
        fields = ('id',
                  'name',
                  'slug',
                  'description',
                  'share_url',
                  'is_public',
                  'image',
                  'creator',)


class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = ('id', 'name')


class ArtistDetailSerializer(serializers.ModelSerializer):
    country = CountrySerializer(many=False, read_only=True)
    tag = TagSerializer(many=False, read_only=True)
    genre = GenreSerializer(many=False, read_only=True)

    class Meta:
        model = Artist
        fields = ('id', 'name', 'image', 'description', 'country', 'genre', 'tag', 'gender')


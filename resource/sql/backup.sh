cd /var/www/muusick/resource/sql 

echo "Backup Muusick DB..."
FILENAME="`date +%Y-%m-%d`_`date +%s`.sql"

echo $FILENAME

mysqldump -u root muusick > $FILENAME

sleep 10

echo "Upload to S3...."


s3cmd put $FILENAME s3://muusick.backup/$FILENAME


sleep 10

rm $FILENAME


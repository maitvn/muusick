console.log('>>>import playlist js')


var state = {};
if (window.location.pathname.indexOf('/add/') > 0){
    state.isAddPage = true;
}
state.isMessagedChanged = false;


django.jQuery(function () {
    var LtNotify = LTNotify = (function () {
      var timeout = null;
      if(!document.getElementById('lt-notify')){
        document.body.innerHTML += '<div id="lt-notify" style="display:none;"></div>';
      }
      var rootEl = document.getElementById('lt-notify');
      rootEl.addEventListener('click', function () {
        rootEl.style.display = 'none';
      });
      rootEl.addEventListener('mouseover', function () {
        clearTimeout(timeout);
      });
      rootEl.addEventListener('mouseout', function () {
        timeout = setTimeout(function () {
          rootEl.style.display = 'none';
        }, 3000);
      });
      return {
        notify: function (msg) {
          clearTimeout(timeout);
          rootEl.innerHTML = msg;
          rootEl.className = "";
          rootEl.style.display = 'inline-block';
          timeout = setTimeout(function () {
            rootEl.style.display = 'none';
          }, 3000);
        },
        error: function (msg) {
          clearTimeout(timeout);
          rootEl.innerHTML = msg;
          rootEl.style.display = 'inline-block';
          rootEl.className = "error";
          timeout = setTimeout(function () {
            rootEl.style.display = 'none';
          }, 4000);
        }
      }
    })();
    var $ = django.jQuery;


    // Click import button
    $('.import-btn').click(function () {
        var self = this;
        var id = $(this).attr('id');
        $(this).text('Importing...').addClass('sending').prop('disabled', true);
        $.post('/backoffice/api/import_playlist', {id: id}, function (res) {
            $(self).text('Send Test').removeClass('sending').prop('disabled', false);
            LtNotify.notify('Done importing playlist!');
        })
    })

    $('#id_type').change(function () {
        showHideShop();
    })

    showHideShop();
    function showHideShop () {
        $type = $('#id_type');

        var $playlist = $('.form-row.field-playlist');
        var $name = $('.form-row.field-new_playlist_name');
        var $desc = $('.form-row.field-new_playlist_description');

        var currentType = $('#id_type option:selected').val();
        if (currentType === '0') {
            // New
            $playlist.hide();
            $name.show();
            $desc.show();
        } else {
            // Update
            $playlist.show();
            $name.hide();
            $desc.hide();
        }
    }
})

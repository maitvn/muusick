
### Ubuntu

  * sudo apt-get install -y libav-tools
  * sudo pip install --upgrade youtube-dl

### Mac
  * brew install libav
  * sudo pip install --upgrade youtube-dl

### Ref
https://github.com/rg3/youtube-dl/blob/master/youtube_dl/YoutubeDL.py#L128-L278

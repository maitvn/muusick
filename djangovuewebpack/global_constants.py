# -*- coding: utf-8 -*-


ACTIVITY_ACTIONS = {
    'official_playlist_release': {
        'type': 'p',
        'template': 'New official playlist released  <b>%s</b>'
    },
    'user_follow_playlist': {
        'type': 'p',
        'template': '<b> %s </b> recently followed <b>%s</b> playlist'
    },
    'user_watch_playlist': {
        'type': 'p',
        'template': '<b> %s </b> recently watched <b>%s</b> playlist'
    },
    'user_publish_playlist': {
        'type': 'p',
        'template': '<b> %s </b> published <b>%s</b> playlist'
    },
}

ACTIVITY_OPTIONS = (
    (a,  a) for a in ACTIVITY_ACTIONS
)
